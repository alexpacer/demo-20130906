﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Project20130906.Libs;
using Project20130906.Libs.Context;

namespace Project20130906.Api.Controllers
{
    public class ProductsController : ApiController
    {
        private ModelContext _db = new ModelContext();

        /// <summary>
        /// List of products by a given language code.
        /// </summary>
        /// <example>
        /// GET http://www.example.com/api/products?&lang=en-US&category_id=1 HTTP/1.1
        ///   
        /// HTTP/1.1 200 OK
        /// Cache-Control: no-cache
        /// Pragma: no-cache
        /// Content-Type: application/json; charset=utf-8
        /// Expires: -1
        /// Server: Microsoft-IIS/7.0
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Date: Thu, 19 Sep 2013 14:26:25 GMT
        /// Content-Length: 267
        ///   
        /// {
        ///     "en-US" : [{
        ///             "id" : 1,
        ///             "name" : "7736034954_133e155b0b",
        ///             "image_url" : "http://www.example.com/api/content/public/products/7736034954_133e155b0b.jpg",
        ///             "short_description" : "Short Description of 7736034954_133e155b0b",
        ///             "full_description" : "Full Description of 7736034954_133e155b0b"
        ///         }
        ///     ]
        /// }
        /// </example>
        /// <param name="category_id">integer id of the category to lookup</param>
        /// <param name="lang">language to filter off results</param>
        /// <returns></returns>
        [HttpGet]
        public object Index(int category_id, string lang)
        {
            var serverPath = Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, "");

            var cat = _db.Categories.FirstOrDefault(x => x.Id == category_id);
            if (cat == null) { throw new HttpResponseException(HttpStatusCode.NotFound); }
            var trans = cat.Translations.FirstOrDefault(x => x.Language == lang);
            if (trans == null) { throw new HttpResponseException(HttpStatusCode.NotFound); }

            var products = new JArray();
            foreach (var product in cat.Products)
            {
                var pTrans = product.Translations.FirstOrDefault(x => x.Language == lang);
                var p = new JObject
                    {
                        { "id", product.Id },
                        { "name", pTrans.Name },
                        { "image_url", serverPath + VirtualPathUtility.ToAbsolute(Conf.ProductImagePath + "/" + product.Image) },
                        { "short_description", pTrans.ShortDescription },
                        { "full_description", pTrans.FullDescription },
                    };
                products.Add(p);
            }

            var ret = new JObject(new JProperty(trans.Language, products));

            return ret;
        }

        /// <summary>
        /// Looks up a specific product by it's unique id
        /// </summary>
        /// <example>
        /// GET http://www.example.com/api/products/1?lang=en-US HTTP/1.1
        /// 
        /// --- 
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: no-cache
        /// Pragma: no-cache
        /// Content-Type: application/json; charset=utf-8
        /// Expires: -1
        /// Server: Microsoft-IIS/7.0
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Date: Thu, 19 Sep 2013 14:28:59 GMT
        /// Content-Length: 403
        /// 
        /// {
        /// 	"en-US" : {
        /// 		"name" : "7736034954_133e155b0b",
        /// 		"image_url" : "http://www.example.com/api/content/public/products/7736034954_133e155b0b.jpg",
        /// 		"short_description" : "Short Description of 7736034954_133e155b0b",
        /// 		"description" : "Full Description of 7736034954_133e155b0b",
        /// 		"remarks" : "Remark of 7736034954_133e155b0b",
        /// 		"category" : [{
        /// 				"name" : "Top Lv 1",
        /// 				"slogan" : null
        /// 			}, {
        /// 				"name" : "Lv 2-1",
        /// 				"slogan" : "Some slogan Lv 2-1"
        /// 			}
        /// 		],
        /// 		"color" : []
        /// 	}
        /// }
        /// </example>
        /// <param name="product_id">the product id to lookup</param>
        /// <param name="lang">language to filter off results</param>
        /// <returns>see example</returns>
        [HttpGet]
        public object Get(int product_id, string lang)
        {
            var serverPath = Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, "");
            var prod = _db.Products.FirstOrDefault(x => x.Id == product_id);
            if (prod == null) { throw new HttpResponseException(HttpStatusCode.NotFound); }

            var trans = prod.Translations.FirstOrDefault(x => x.Language == lang);
            if (trans == null) { throw new HttpResponseException(HttpStatusCode.NotFound); }

            var categories = new JArray();
            foreach (var c in prod.Categories)
            {
                var cTrans = c.Translations.FirstOrDefault(x => x.Language == lang);

                categories.Add(new JObject
                    {
                        {"name",cTrans.Name},
                        {"slogan",cTrans.Slogan},
                    });
            }

            var colors = new JArray();
            foreach (var c in prod.ColorCards)
            {
                colors.Add(new JObject
                    {
                        {"url", serverPath + VirtualPathUtility.ToAbsolute(Conf.ColorCardImagePath + "/" + c.Image)}
                    });
            }

            var product = new JObject
                {
                    {"name", trans.Name},
                    {"image_url", serverPath + VirtualPathUtility.ToAbsolute(Conf.ProductImagePath + "/" + prod.Image)},
                    {"short_description", trans.ShortDescription },
                    {"description", trans.FullDescription},
                    {"remarks", trans.Remark },
                    {"category", categories},
                    {"color", colors}
                };

            var ret = new JObject(new JProperty(trans.Language, product));
            return ret;
        }
    }
    }
}
