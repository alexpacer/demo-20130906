﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Project20130906.Libs;
using Project20130906.Libs.Context;
using Project20130906.Libs.Entities;

namespace Project20130906.Api.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly ModelContext _db = new ModelContext();

        /// <summary>
        /// Recursively fill in sub categories as child json objet and returns self as an JObject Category 
        /// </summary>
        /// <param name="cat">the category to parse</param>
        /// <param name="lang">the language to filter off data</param>
        /// <returns></returns>
        private JObject GetSubCategories(Category cat, string lang)
        {
            var serverPath = Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, "");
            var trans = cat.Translations.FirstOrDefault(x => x.Language == lang);

            if (trans == null)
            {
                trans = cat.Translations.FirstOrDefault(x => x.Language == "en-US") ?? cat.Translations.FirstOrDefault();
            }

            var self = new JObject
                {
                    { "category_id", cat.Id },
                    { "order", cat.Order },
                    { "name", trans.Name },
                    { "slogan", trans.Slogan },
                    { "description", trans.Description },
                    { "background_image_path", serverPath + VirtualPathUtility.ToAbsolute(Conf.CategoryBackgroundPath + "/" + cat.BackgroundImage) },
                    { "image_path", serverPath + VirtualPathUtility.ToAbsolute(Conf.CategoryCoverImagePath + "/" + cat.CoverImage) }
                };

            if (cat.Children.Count > 0)
            {
                var arr = new JArray();
                foreach (var a in cat.Children)
                {
                    arr.Add(GetSubCategories(a, lang));
                }
                self.Add(new JProperty("sub_category", arr));
            }

            return self;
        }

        /// <summary>
        /// Returns all categories as a tree-like hirechy, the result will include all languages available and as long as each category's information.
        /// </summary>
        /// <example>
        /// GET http://www.example.com/api/categories/ HTTP/1.1
        /// 
        /// ---- 
        /// 
        /// HTTP/1.1 200 OK
        /// Cache-Control: no-cache
        /// Pragma: no-cache
        /// Content-Type: application/json; charset=utf-8
        /// Expires: -1
        /// Server: Microsoft-IIS/7.0
        /// X-AspNet-Version: 4.0.30319
        /// X-Powered-By: ASP.NET
        /// Date: Thu, 19 Sep 2013 14:31:24 GMT
        /// Content-Length: 1765
        /// 
        /// {
        /// 	"en-US" : {
        /// 		"main_categories" : [{
        /// 				"name" : "Top Lv 1",
        /// 				"slogan" : null,
        /// 				"description" : null,
        /// 				"background_image_path" : "http://www.example.com/api/content/public/category-background/10636583-md.jpg",
        /// 				"image_path" : "http://www.example.com/api/content/public/category-cover/164926.jpg",
        /// 				"sub_category" : [{
        /// 						"name" : "Lv 2-1",
        /// 						"slogan" : "Some slogan Lv 2-1",
        /// 						"description" : "Some description Lv 2-1",
        /// 						"background_image_path" : "http://www.example.com/api/content/public/category-background/245405_original.jpg",
        /// 						"image_path" : "http://www.example.com/api/content/public/category-cover/210808166.jpg"
        /// 					}, {
        /// 						"name" : "Lv 2 - 2",
        /// 						"slogan" : "Some slogan Lv 2-2",
        /// 						"description" : "Some description Lv 2-2",
        /// 						"background_image_path" : "http://www.example.com/api/content/public/category-background/6789883-md.jpg",
        /// 						"image_path" : "http://www.example.com/api/content/public/category-cover/392419.jpg",
        /// 						"sub_category" : [{
        /// 								"name" : "Lv 3 - 1",
        /// 								"slogan" : "Some slogan Lv 3-1",
        /// 								"description" : "Some description Lv 3-1",
        /// 								"background_image_path" : "http://www.example.com/api/content/public/category-background/763729_original.jpg",
        /// 								"image_path" : "http://www.example.com/api/content/public/category-cover/5th-anniversary-banner.png"
        /// 							}, {
        /// 								"name" : "Lv 3 - 2",
        /// 								"slogan" : "Some slogan Lv 3-2",
        /// 								"description" : "Some description Lv 3-2",
        /// 								"background_image_path" : "http://www.example.com/api/content/public/category-background/7707207-md.jpg",
        /// 								"image_path" : "http://www.example.com/api/content/public/category-cover/life2-2_20130828600_5.jpg"
        /// 							}
        /// 						]
        /// 					}, {
        /// 						"name" : "Lv 2 - 3",
        /// 						"slogan" : "Some slogan Lv 2-3",
        /// 						"description" : "Some description Lv 2-3",
        /// 						"background_image_path" : "http://www.example.com/api/content/public/category-background/7966131-lg.jpg",
        /// 						"image_path" : "http://www.example.com/api/content/public/category-cover/sp2_20130916600_17.jpg"
        /// 					}
        /// 				]
        /// 			}
        /// 		]
        /// 	},
        /// 	"zh-TW" : {
        /// 		"main_categories" : []
        /// 	},
        /// 	"zh-CN" : {
        /// 		"main_categories" : []
        /// 	}
        /// }
        /// </example>
        /// <returns>see example</returns>
        [HttpGet]
        public object AllCategories()
        {
            JObject ret = new JObject();
            foreach (var l in Conf.LanguageCodes)
            {
                var arr = new JArray();
                foreach (var a in _db.Categories
                    .Where(c => c.Translations.Any(bb => bb.Language == l) && c.Parent == null)
                    .OrderBy(x => x.Order))
                {
                    arr.Add(GetSubCategories(a, l));
                }
                var main_categories = new JObject(
                    new JProperty("main_categories", arr));
                ret.Add(new JProperty(l, main_categories));
            }

            return ret;
        }
    }
}
