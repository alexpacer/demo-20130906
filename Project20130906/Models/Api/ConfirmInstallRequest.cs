﻿namespace Project20130906.Models.Api
{
    public class ConfirmInstallRequest
    {
        public string confirm { get; set; }
        public string pwd { get; set; }
    }
}