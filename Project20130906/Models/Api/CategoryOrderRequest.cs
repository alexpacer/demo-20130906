﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project20130906.Models.Api
{
    public class CategoryOrderRequest
    {
        public int id { get; set; }

        public int order { get; set; }
    }
}