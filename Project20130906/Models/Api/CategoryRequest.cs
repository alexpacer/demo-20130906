﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project20130906.Models.Api
{
    public class CategoryRequest
    {
        public int id { get; set; }

        public int order { get; set; }

        public int? parent_id { get; set; }

        public string background_image_file { get; set; }

        public string background_image_thumb_file { get; set; }

        public string uploaded_images { get; set; }

        public string name { get; set; }

        public string language { get; set; }
    }
}