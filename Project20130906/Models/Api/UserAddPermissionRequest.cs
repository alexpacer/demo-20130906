﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project20130906.Models.Api
{
    public class UserAddPermissionRequest
    {
        public string user_id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int p { get; set; }
    }

    public class UsersAddPermissionRequest
    {
        public string user_id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int p { get; set; }
    }
}