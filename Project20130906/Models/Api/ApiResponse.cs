﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project20130906.Models.Api
{
    public class ApiResponse
    {
        public string status { get { return _status; } set { _status = value; } }
        private string _status = "success";

        public string message { get; set; }
    }
}