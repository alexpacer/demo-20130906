﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project20130906.Libs.Entities;

namespace Project20130906.Models.ViewModels
{
    public class ColorCardPageVm
    {
        public IEnumerable<ColorCard> ColorCards { get; set; }
        public IEnumerable<ColorCardGroup> EmptyGroups { get; set; }
    }
}
