﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project20130906.Models.ViewModels
{
    public class PagingVm
    {
        public PagingVm(int totalRecords, int totalPages, int currentPage)
        {
            TotalRecords = totalRecords;
            CurrentPage = currentPage;
            TotalPages = totalPages;
        }

        public int TotalRecords { get; private set; }
        public int TotalPages { get; private set; }
        public int CurrentPage { get; private set; }
        public int? NextPage { get { return (CurrentPage >= TotalPages) ? (int?)null : CurrentPage + 1; } }
        public int? PrevPage { get { return (CurrentPage <= 1) ? (int?) null : CurrentPage - 1; } }
    }
}