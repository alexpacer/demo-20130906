﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project20130906.Models.ViewModels
{
    public class NewsIndexVm
    {
        public IEnumerable<NewsPostVm> NewsPosts { get; set; }

        public PagingVm Paging { get; set; }
    }

    public class NewsPostVm
    {
        public int Id { get; set; }
        public NewsPostVmTranslation Translation { get; set; }
        public DateTime CreateAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class NewsPostVmTranslation
    {
        public string Subject { get; set; }
        [AllowHtml]
        public string Description { get; set; }
    }

}