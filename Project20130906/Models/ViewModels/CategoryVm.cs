﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project20130906.App_LocalResources.Views;

namespace Project20130906.Models.ViewModels
{
    public class CategoryVm
    {
        public int Id { get; set; }

        public int Order { get; set; }

        public int? ParentId { get; set; }

        public string BackgroundImage { get; set; }
        public string BackgroundImageThumb { get; set; }

        public string CoverImage { get; set; }
        public string CoverImageThumb { get; set; }

        public string SloganImage { get; set; }
        public string SloganImageThumb { get; set; }

        public DateTime CreateAt { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime? UpdatedAt { get; set; }



        public string Name { get; set; }
        public string Slogan { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        public string Language { get; set; }

        public HttpPostedFileBase background_image { get; set; }
        public HttpPostedFileBase cover_image { get; set; }
        public HttpPostedFileBase slogan_image { get; set; }
    }
}