﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project20130906.Libs.Entities;

namespace Project20130906.Models.ViewModels
{
    public class ProductVm
    {
        public ProductVm()
        {
            CurrentCategories = new Collection<CategoryVm>();
            CurrentColorCards = new Collection<ColorCardVm>();
            categories = new List<int>();
            color_cards = new List<int>();
        }

        public int Id { get; set; }
        public int? CategoryId { get; set; }
        public DateTime CreateAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateAt { get; set; }

        public string Image { get; set; }
        public string ImageThumb { get; set; }

        public string Name { get; set; }
        public string ShortDescription { get; set; }
        [AllowHtml]
        public string FullDescription { get; set; }
        [AllowHtml]
        public string Remark { get; set; }

        public virtual ICollection<CategoryVm> CurrentCategories { get; set; }
        public virtual ICollection<ColorCardVm> CurrentColorCards { get; set; } 

        public virtual List<ColorCard> AllColorCards { get; set; }
        public virtual List<Category> AllCategories { get; set; }

        public HttpPostedFileBase submit_image { get; set; }
        public List<int> categories { get; set; }
        public List<int> color_cards { get; set; }
        public string language { get; set; }
    }
    
}