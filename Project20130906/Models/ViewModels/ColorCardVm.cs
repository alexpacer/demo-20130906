﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project20130906.Models.ViewModels
{
    public class ColorCardVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string ImageThumb { get; set; }
        public DateTime CreateAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateAt { get; set; }
        public ColorCardGroupVm CurrentGroup { get; set; }

        public HttpPostedFileBase submit_image { get; set; }
        public int? group { get; set; }
    }
}