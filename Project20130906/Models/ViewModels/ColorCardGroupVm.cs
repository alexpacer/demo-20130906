﻿using System.Web;

namespace Project20130906.Models.ViewModels
{
    public class ColorCardGroupVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public string Image { get; set; }
        public string ImageThumb { get; set; }

        public HttpPostedFileBase cover_image { get; set; }
    }
}