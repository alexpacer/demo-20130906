﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project20130906.App_LocalResources.Views;
using Project20130906.Controllers.Filters;
using Project20130906.Libs;
using Project20130906.Libs.Context;
using Project20130906.Libs.Entities;
using Project20130906.Models.Api;
using Project20130906.Models.ViewModels;

namespace Project20130906.Controllers
{
    using Project20130906.Helpers;
    using Project20130906.Models.Api;

    [ApiInit]
    public partial class WebApiController : ApiController
    {
        private readonly ModelContext _db = new ModelContext();

        /// <summary>
        /// Controller entry point
        /// </summary>
        public void Initialize()
        {
            AutoMapper.Mapper.CreateMap<Category, CategoryApiVm>();
        }

        [HttpPost]
        public ApiResponse LanguagePreference(LanguagePreferenceRequest request)
        {
            var p = request.p;
            var response = new ApiResponse();

            if (!string.IsNullOrEmpty(p))
            {
                switch (p.ToLowerInvariant())
                {
                    default:
                        response.status = "error";
                        response.message = "Incorrect language preference.";
                        break;
                    case "en-us":
                        SessionHelper.CurrentLanguage = "en-US";
                        break;
                    case "zh-tw":
                        SessionHelper.CurrentLanguage = "zh-TW";
                        break;
                    case "zh-cn":
                        SessionHelper.CurrentLanguage = "zh-CN";
                        break;
                }
            }
            else
            {
                response.status = "error";
                response.message = "Incorrect language preference.";
            }

            return response;
        }

        [HttpGet]
        public object UserLookup(string username)
        {
            //using (var context = new PrincipalContext(ContextType.Domain, "tw.trendnet.org"))
            PrincipalContext principal;
            if (ConfigurationManager.AppSettings["UserLookupMode"] == "domain")
            {
                principal = new PrincipalContext(ContextType.Domain);
            }
            else
            {
                principal = new PrincipalContext(ContextType.Machine);
            }

            using (var context = principal)
            {
                using (var user = UserPrincipal.FindByIdentity(context, username))
                {
                    if (user == null)
                    {
                        return new ApiResponse { status = "error", message = Permissions.Error_UserNotFound };
                    }

                    NTAccount ntAccount = (NTAccount)user.Sid.Translate(typeof(NTAccount));

                    var response = new JObject{ 
                        { "status", "success"},
                        { "user_id", ntAccount.ToString() },
                        { "email", user.EmailAddress },
                        { "name", string.Format("{0} {1}", user.GivenName, user.Surname)}
                    };

                    return response;
                }
            }


        }

        [HttpPost]
        public object UserAddPermission(UserAddPermissionRequest request)
        {
            var user = _db.Users.FirstOrDefault(x => x.Id == request.user_id);
            var permission = _db.Permissions.FirstOrDefault(p => p.Id == request.p);

            if (user == null)
            {
                user = new User { Id = request.user_id, Email = request.email, Password = request.password, Permissions = new Collection<Permission>() };
                _db.Users.Add(user);
            }

            if (!user.Permissions.Any(x => x.Id == request.p))
            {
                user.Permissions.Add(permission);
                _db.SaveChanges();

                return new JObject
                    {
                        {"status", "success" },
                        {"message", Permissions.Notice_PermissionAssigned }
                    };
            }

            return new JObject
                    {
                        { "status", "neutral" },
                        { "message", Permissions.Notice_PermissionAlreadyAssigned }
                    };
        }

        [HttpPost]
        public object UsersAddPermission(HttpRequestMessage req)
        {
            var reqStr = req.Content.ReadAsStringAsync().Result;
            IEnumerable<UsersAddPermissionRequest> request = JsonConvert.DeserializeObject < IEnumerable < UsersAddPermissionRequest >> (reqStr);
        
            var assigned = 0;
            foreach (var usr in request)
            {
                var user = _db.Users.FirstOrDefault(x => x.Id.EndsWith(usr.user_id));
                var permission = _db.Permissions.FirstOrDefault(p => p.Id == usr.p);

                if (user == null)
                {
                    user = new User { Id = string.Format("\\" + usr.user_id), Email = usr.email, Password = usr.password, Permissions = new Collection<Permission>() };
                    _db.Users.Add(user);
                }

                if (!user.Permissions.Any(x => x.Id == usr.p))
                {
                    user.Permissions.Add(permission);
                    
                }
                assigned++;
            }

            if (assigned != 0)
            {
                _db.SaveChanges();
                return new JObject
                    {
                        {"status", "success" },
                        {"message", Permissions.Notice_PermissionAssigned }
                    };
            }

            return new JObject
                    {
                        { "status", "neutral" },
                        { "message", Permissions.Notice_PermissionAlreadyAssigned }
                    };
        }

        [HttpPost]
        public ApiResponse RemoveUserPermission(IEnumerable<RemoveUserPermission> request)
        {
            foreach (var p in request)
            {
                var permission = _db.Permissions.FirstOrDefault(x => x.Id == p.id);
                if (permission != null){
                    var usersToRemove = permission.Users.Where(u => p.users.Contains(u.Id)).ToList();
                    foreach (var user in usersToRemove){
                        permission.Users.Remove(user);
                    }
                }
            }

            if (_db.SaveChanges() > 0)
            {
                return new ApiResponse(){status = "success", message = Permissions.Notice_PermissionRemoved};
            }
            else
            {
                return new ApiResponse(){ status = "error", message = Permissions.Error_UnableToRemoveUsers };
            }
        }

        [HttpDelete]
        public ApiResponse NewsDelete(int id)
        {
            var errObj = new ApiResponse { status = "error" };
            var news = _db.NewsPosts.FirstOrDefault(x => x.Id == id);

            if (news == null)
            {
                errObj.message = "Item not found.";
                return errObj;
            }

            _db.NewsPosts.Remove(news);
            _db.SaveChanges();

            return new ApiResponse { status = "success", message = News.Notice_NewsPostDeletedSuccessfully };
        }

        [HttpPost]
        public ApiResponse CommitInstall(ConfirmInstallRequest request)
        {
            try
            {
                if (request.pwd == ConfigurationManager.AppSettings["CommitDbInstallPwd"])
                {
                    _db.Database.ExecuteSqlCommand("EXEC sp_MSforeachtable 'DELETE FROM ?'");
                    this.InitDbData();

                    return new ApiResponse {status = "success", message = "Initialized database for sample data."};
                }

                return new ApiResponse {status = "error"};
            }
            catch (Exception ex)
            {
                return new ApiResponse {status = "error", message = ex.ToString()};
            }
        }

        [HttpDelete]
        public ApiResponse DeleteProduct(int id)
        {
            string sql = @"
DELETE FROM ProductsToCategories WHERE ProductId = @ProductId;
  DELETE FROM ProductsToColorCards WHERE ProductId = @ProductId;
  DELETE FROM Products WHERE Id = @ProductId;
";
            SqlParameter pProductId = new SqlParameter("@ProductId", SqlDbType.Int) { Value = id };

            var conn = _db.Database.Connection;

            conn.Open();
            var cmd = conn.CreateCommand();
            cmd.CommandText = sql;
            cmd.Parameters.Add(pProductId);
            
            var ret = new List<Guid>();

            if (cmd.ExecuteNonQuery() > 0)
                return new ApiResponse() { message = Products.Notice_ProductDeletionSuccessfully };

            return new ApiResponse { status = "error", message = Products.Error_UnableToDeleteProduct }; 
        }

        private void InitDbData()
        {
            var defLanguage = "en-US";
            #region Create Default user
            var usrs = new Collection<User>
                {
                    new User { Id = "DOMAIN\\service_account", Password = "", Email = "user@example.com" },
                    new User { Id = @"TREND\alex", Email = "user@example.com" },
                    //new User { Id = @"Alex-PC\Alex", Email = "user@example.com" }
                };
            var admin = usrs.First(x => x.Id == "DOMAIN\\service_account");
            foreach (var user in usrs) { _db.Users.Add(user); }

            #endregion

            #region Create deafult permissions
            /*
             * God can do all following:
                a) Who can add/edit product  -- [Management]
                b) Who can delete product    -- Amendment
                c) Who can add/edit category -- [Management]
                d) Who can delete category   -- Amendment
                e) Who can add/edit news     -- [Management]
                f) Who can delete news       -- Amendment
             */
            var permissionGod = new Permission { Id = (int)Def.Permissions.God, Name = "God", IsMenu = false, CreateAt = DateTime.UtcNow, CreatedBy = admin.Id };
            var permissionProdManage = new Permission { Id = (int)Def.Permissions.Product_Management, Name = "Product_Management", IsMenu = true, Route = "products", CreateAt = DateTime.UtcNow, CreatedBy = admin.Id };
            _db.Permissions.Add(permissionGod);
            _db.Permissions.Add(permissionProdManage);
            var p3 = new Permission
                {
                    Id = (int)Def.Permissions.Product_Amendment,
                    Name = "Product_Amendment",
                    IsMenu = false,
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p3);
            var p4 = new Permission
                {
                    Id = (int)Def.Permissions.Category_Management,
                    Name = "Category_Management",
                    IsMenu = true,
                    Route = "categories",
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p4);
            var p5 = new Permission
                {
                    Id = (int)Def.Permissions.Category_Amendment,
                    Name = "Category_Amendment",
                    IsMenu = false,
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p5);
            var p6 = new Permission
                {
                    Id = (int)Def.Permissions.Color_Card_Management,
                    Name = "Color_Card_Management",
                    IsMenu = true,
                    Route = "color-cards",
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p6);

            var p7 = new Permission
                {
                    Id = (int)Def.Permissions.News_Management,
                    Name = "News_Management",
                    IsMenu = true,
                    Route = "news",
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p7);

            var p8 = new Permission
                {
                    Id = (int)Def.Permissions.News_Amendment,
                    Name = "News_Amendment",
                    IsMenu = false,
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p8);

            var p9 = new Permission
                {
                    Id = (int)Def.Permissions.Outlook_Web_Access,
                    Name = "Outlook_Web_Access",
                    IsMenu = true,
                    Route = "#outlook",
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p9);

            var p10 = new Permission
                {
                    Id = (int)Def.Permissions.Manage_Permissions,
                    Name = "Manage_Permissions",
                    IsMenu = true,
                    Route = "permissions",
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id
                };
            _db.Permissions.Add(p10);
            #endregion

            #region Create Colors
            
            var ccardSampleDir = new DirectoryInfo(HostingEnvironment.MapPath(Conf.SampleColorCardDirectory));

            int c = 1;
            foreach (var f in ccardSampleDir.GetFiles())
            {
                var targetFile =
                    HostingEnvironment.MapPath(Conf.ColorCardImagePath + Path.DirectorySeparatorChar + f.Name);
                if (!new FileInfo(targetFile).Exists) f.CopyTo(targetFile);

                var thumbFileName = string.Format(@"{0}_thumb.{1}", f.Name.Split('.')[0], f.Name.Split('.')[1]);
                // create thumb
                using (var img = Image.FromFile(f.FullName))
                {
                    //original height / original width x new width = new height
                    var newWidth = Conf.ColorCardThumbnailSize;
                    var aa = (decimal)img.Height / (decimal)img.Width * newWidth;
                    var newHeight = (int)Decimal.Round(aa, 0, MidpointRounding.AwayFromZero);

                    using (var thumb = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero))
                    {
                        thumb.Save(HostingEnvironment.MapPath(Conf.ColorCardImagePath + Path.DirectorySeparatorChar + thumbFileName));
                    }
                }

                var ccard = new ColorCard
                {
                    Name = "Color card " + c.ToString(),
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id,
                    Image = f.Name,
                    ImageThumb = thumbFileName
                };
                _db.ColorCards.Add(ccard);
                c++;
            }
            #endregion

            #region Setup User's permission
            admin.Permissions = new Collection<Permission> { permissionGod, permissionProdManage, p4, p6, p7, p9, p10 };
            #endregion

            #region Carete Categories

            var catBackgroundImageDir = new DirectoryInfo(HostingEnvironment.MapPath(Conf.SampleCategoryBackgroundDirectory));
            var catCoverImageDir = new DirectoryInfo(HostingEnvironment.MapPath(Conf.SampleCategoryCoverDirectory));

            var rootCategory = new Category
            {
                BackgroundImage = catBackgroundImageDir.GetFiles()[0].Name,
                BackgroundImageThumb = catBackgroundImageDir.GetFiles()[0].Name.Split('.').First() + "_thumb" + catBackgroundImageDir.GetFiles()[0].Extension,
                CoverImage = catCoverImageDir.GetFiles()[0].Name,
                CoverImageThumb = string.Format(@"{0}_thumb{1}", catCoverImageDir.GetFiles()[0].Name.Split('.').First(), catCoverImageDir.GetFiles()[0].Extension),
                Translations = new Collection<CategoryTranslation>
                    {
                        new CategoryTranslation { Name = "Top Lv 1", Language = defLanguage },
                        new CategoryTranslation { Name = "分類層1", Language = "zh-TW" },
                        new CategoryTranslation { Name = "分类层1", Language = "zh-CN" }
                    },
                CreateAt = DateTime.UtcNow,
                CreatedBy = admin.Id,
                Children = new Collection<Category>
                    {
                        new Category {
                            BackgroundImage = catBackgroundImageDir.GetFiles()[1].Name,
                            BackgroundImageThumb = catBackgroundImageDir.GetFiles()[1].Name.Split('.').First() + "_thumb" +catBackgroundImageDir.GetFiles()[1].Extension,
                            CoverImage = catCoverImageDir.GetFiles()[1].Name,
                            CoverImageThumb = string.Format(@"{0}_thumb{1}", catCoverImageDir.GetFiles()[1].Name.Split('.').First(), catCoverImageDir.GetFiles()[1].Extension),
                            Translations = new Collection<CategoryTranslation>
                                {
                                    new CategoryTranslation { Name = "Lv 2-1", Language = defLanguage, Slogan = "Some slogan Lv 2-1", Description = "Some description Lv 2-1"},
                                    new CategoryTranslation { Name = "分類層2-1", Language = "zh-TW", Slogan = "分類層2-1 slogan", Description = "分類層2-1註解"},
                                    new CategoryTranslation { Name = "分类层2-1", Language = "zh-CN", Slogan = "分类层2-1 slogan", Description = "分类层2-1注解"}
                                },
                            CreateAt = DateTime.UtcNow, 
                            CreatedBy = admin.Id },
                        new Category { 
                            BackgroundImage = catBackgroundImageDir.GetFiles()[2].Name,
                            BackgroundImageThumb = catBackgroundImageDir.GetFiles()[2].Name.Split('.').First() + "_thumb" +catBackgroundImageDir.GetFiles()[2].Extension,
                            CoverImage = catCoverImageDir.GetFiles()[2].Name,
                            CoverImageThumb = string.Format(@"{0}_thumb{1}", catCoverImageDir.GetFiles()[2].Name.Split('.').First(), catCoverImageDir.GetFiles()[2].Extension),
                            Translations = new Collection<CategoryTranslation>
                                {
                                    new CategoryTranslation { Name = "Lv 2 - 2", Language = defLanguage, Slogan = "Some slogan Lv 2-2", Description = "Some description Lv 2-2"},
                                    new CategoryTranslation { Name = "分類層2-2", Language = "zh-TW", Slogan = "分類層2-2 slogan", Description = "分類層2-2註解"},
                                    new CategoryTranslation { Name = "分类层2-2", Language = "zh-CN", Slogan = "分类层2-2 slogan", Description = "分类层2-2注解"}
                                }, 
                            CreateAt = DateTime.UtcNow, CreatedBy = admin.Id },
                            //Children = new Collection<Category>{
                            //        
                            //    }
                            //},
                        new Category{ 
                            BackgroundImage = catBackgroundImageDir.GetFiles()[5].Name,
                            BackgroundImageThumb = catBackgroundImageDir.GetFiles()[5].Name.Split('.').First() + "_thumb" +catBackgroundImageDir.GetFiles()[5].Extension,
                            CoverImage = catCoverImageDir.GetFiles()[5].Name,
                            CoverImageThumb = string.Format(@"{0}_thumb{1}", catCoverImageDir.GetFiles()[5].Name.Split('.').First(), catCoverImageDir.GetFiles()[5].Extension),
                            Translations = new Collection<CategoryTranslation>
                                {
                                    new CategoryTranslation { Name = "Lv 2 - 3", Language = defLanguage, Slogan = "Some slogan Lv 2-3", Description = "Some description Lv 2-3"},
                                    new CategoryTranslation { Name = "分類層2-3", Language = "zh-TW", Slogan = "分類層2-3 slogan", Description = "分類層2-3註解"},
                                    new CategoryTranslation { Name = "分类层2-3", Language = "zh-CN", Slogan = "分类层2-3 slogan", Description = "分类层2-3注解"}
                                }, 
                            CreateAt = DateTime.UtcNow, 
                            CreatedBy = admin.Id },

                    }
            };

            var secondNodeCategory = new Category
                {
                    BackgroundImage = catBackgroundImageDir.GetFiles()[3].Name,
                    BackgroundImageThumb =
                        catBackgroundImageDir.GetFiles()[3].Name.Split('.').First() + "_thumb" +
                        catBackgroundImageDir.GetFiles()[3].Extension,
                    CoverImage = catCoverImageDir.GetFiles()[3].Name,
                    CoverImageThumb = string.Format(@"{0}_thumb{1}", catCoverImageDir.GetFiles()[3].Name.Split('.').First(),
                                      catCoverImageDir.GetFiles()[3].Extension),
                    Translations = new Collection<CategoryTranslation>
                        {
                            new CategoryTranslation{ Name = "Lv 1 - 2", Language = defLanguage, Slogan = "Some slogan Lv 1-2", Description = "Some description Lv 1-2" },
                            new CategoryTranslation{ Name = "分類層1-2", Language = "zh-TW", Slogan = "分類層1-2 slogan", Description = "分類層1-2註解" },
                            new CategoryTranslation{ Name = "分类层2-2", Language = "zh-CN", Slogan = "分类层2-2 slogan", Description = "分类层2-2注解" }
                        },
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id,
                    Children = new Collection<Category>{
                            new Category
                                {
                                    BackgroundImage = catBackgroundImageDir.GetFiles()[4].Name,
                                    BackgroundImageThumb =
                                        catBackgroundImageDir.GetFiles()[4].Name.Split('.').First() + "_thumb" +
                                        catBackgroundImageDir.GetFiles()[4].Extension,
                                    CoverImage = catCoverImageDir.GetFiles()[4].Name,
                                    CoverImageThumb =
                                        string.Format(@"{0}_thumb{1}",
                                                      catCoverImageDir.GetFiles()[4].Name.Split('.').First(),
                                                      catCoverImageDir.GetFiles()[4].Extension),
                                    Translations =
                                        new Collection<CategoryTranslation>
                                            {
                                                new CategoryTranslation{ Name = "Lv 2 - 1", Language = defLanguage, Slogan = "Some slogan Lv 2-1", Description = "Some description Lv 2-1" },
                                                new CategoryTranslation{ Name = "分類層2-1", Language = "zh-TW", Slogan = "分類層2-1 slogan", Description = "分類層2-1註解" },
                                                new CategoryTranslation{ Name = "分类层2-1", Language = "zh-CN", Slogan = "分类层2-1 slogan", Description = "分类层2-1注解" }
                                            },
                                    CreateAt = DateTime.UtcNow,
                                    CreatedBy = admin.Id
                                }
                        }
                };

            foreach (var f in catBackgroundImageDir.GetFiles())
            {
                var targetFile =
                    HostingEnvironment.MapPath(Conf.CategoryBackgroundPath + Path.DirectorySeparatorChar + f.Name);
                if(!new FileInfo(targetFile).Exists) f.CopyTo(targetFile);

                // create thumb
                using (var img = Image.FromFile(f.FullName))
                {
                    //original height / original width x new width = new height
                    var newWidth = Conf.CategoryBackgroundThumbnailSize;
                    var aa = (decimal)img.Height / (decimal)img.Width * newWidth;
                    var newHeight = (int)Decimal.Round(aa, 0, MidpointRounding.AwayFromZero);
                    var newFileName = string.Format(@"{0}_thumb.{1}", f.Name.Split('.')[0], f.Name.Split('.')[1]);

                    using (var thumb = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero))
                    {
                        thumb.Save(HostingEnvironment.MapPath(Conf.CategoryBackgroundPath + Path.DirectorySeparatorChar + newFileName));
                    }
                }

            }

            foreach (var f in catCoverImageDir.GetFiles())
            {
                var targetFile =
                    HostingEnvironment.MapPath(Conf.CategoryCoverImagePath + Path.DirectorySeparatorChar + f.Name);
                if (!new FileInfo(targetFile).Exists) f.CopyTo(targetFile);
                
                // create thumb
                using (var img = Image.FromFile(f.FullName))
                {
                    //original height / original width x new width = new height
                    var newWidth = Conf.CategoryCoverThumbnailSize;
                    var aa = (decimal)img.Height / (decimal)img.Width * newWidth;
                    var newHeight = (int)Decimal.Round(aa, 0, MidpointRounding.AwayFromZero);
                    var newFileName = string.Format(@"{0}_thumb.{1}", f.Name.Split('.')[0], f.Name.Split('.')[1]);

                    using (var thumb = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero))
                    {
                        thumb.Save(HostingEnvironment.MapPath(Conf.CategoryCoverImagePath + Path.DirectorySeparatorChar + newFileName));
                    }
                }
            }
            _db.Categories.Add(rootCategory);
            _db.Categories.Add(secondNodeCategory);
            #endregion

            #region Sample Products

            foreach (var f in new DirectoryInfo(HostingEnvironment.MapPath(Conf.SampleProductImageDirectory)).GetFiles())
            {
                var targetFile =
                    HostingEnvironment.MapPath(Conf.ProductImagePath + Path.DirectorySeparatorChar + f.Name);
                if (!new FileInfo(targetFile).Exists) f.CopyTo(targetFile);

                var newThumbName = string.Format(@"{0}_thumb.{1}", f.Name.Split('.')[0], f.Name.Split('.')[1]);
                // create thumb
                using (var img = Image.FromFile(f.FullName))
                {
                    //original height / original width x new width = new height
                    var newWidth = Conf.ProductImageThumbnailSize;
                    var aa = (decimal)img.Height / (decimal)img.Width * newWidth;
                    var newHeight = (int)Decimal.Round(aa, 0, MidpointRounding.AwayFromZero);
                    

                    using (var thumb = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero))
                    {
                        thumb.Save(HostingEnvironment.MapPath(Conf.ProductImagePath + Path.DirectorySeparatorChar + newThumbName));
                    }
                }

                var sampleProduct1 = new Product
                {
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = admin.Id,
                    Categories = new Collection<Category>{
                            rootCategory.Children.FirstOrDefault(),
                            rootCategory
                        },
                    Image = Path.GetFileName(targetFile),
                    ImageThumb = Path.GetFileName(newThumbName),
                    Translations = new Collection<ProductTranslation>{
                            new ProductTranslation{
                                    Name = Path.GetFileNameWithoutExtension(targetFile), 
                                    ShortDescription = "Short Description of " + Path.GetFileNameWithoutExtension(targetFile), 
                                    FullDescription = "Full Description of " + Path.GetFileNameWithoutExtension(targetFile), 
                                    Remark = "Remark of " + Path.GetFileNameWithoutExtension(targetFile), 
                                    Language = defLanguage
                                },
                            new ProductTranslation{
                                    Name = Path.GetFileNameWithoutExtension(targetFile), 
                                    ShortDescription = "簡介 " + Path.GetFileNameWithoutExtension(targetFile), 
                                    FullDescription = "詳細介紹 " + Path.GetFileNameWithoutExtension(targetFile), 
                                    Remark = "註解 " + Path.GetFileNameWithoutExtension(targetFile), 
                                    Language = "zh-TW"
                                },
                            new ProductTranslation{
                                    ShortDescription = "简介 " + Path.GetFileNameWithoutExtension(targetFile), 
                                    FullDescription = "详细介绍 " + Path.GetFileNameWithoutExtension(targetFile), 
                                    Remark = "注解 " + Path.GetFileNameWithoutExtension(targetFile), 
                                    Language = "zh-CN"
                                }
                        }
                };


                _db.Products.Add(sampleProduct1);

            }

            

            #endregion

            #region Sample Posts

            var samplePost1 = new NewsPost
            {
                CreateAt = DateTime.UtcNow,
                CreatedBy = admin.Id,
                Translations = new Collection<NewsPostTranslation>
                        {
                            new NewsPostTranslation { Subject = "Sample News 1", Description = "Sample News 1's Description", Language = defLanguage },
                            new NewsPostTranslation { Subject = "測試公告 1", Description = "測試公告內文", Language = "zh-TW" },
                            new NewsPostTranslation { Subject = "测试公告 1", Description = "测试公告内文", Language = "zh-CN" }
                        }
            };

            var samplePost2 = new NewsPost
            {
                CreateAt = DateTime.UtcNow,
                CreatedBy = admin.Id,
                Translations = new Collection<NewsPostTranslation>
                        {
                            new NewsPostTranslation { Subject = "Sample News 2", Description = "Sample News 2's Description", Language = defLanguage },
                            new NewsPostTranslation { Subject = "測試公告 2", Description = "測試公告內文", Language = "zh-TW" },
                            new NewsPostTranslation { Subject = "测试公告 2", Description = "测试公告内文", Language = "zh-CN" }
                        }
            };
            _db.NewsPosts.Add(samplePost1);
            _db.NewsPosts.Add(samplePost2);

            #endregion

            
            _db.SaveChanges();
        }
    }

}
