﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project20130906.Controllers.Filters;
using Project20130906.Libs;
using Project20130906.Libs.Entities;
using Project20130906.Models.ViewModels;
using Project20130906.Helpers;
using Views=Project20130906.App_LocalResources.Views;

namespace Project20130906.Controllers
{
    public class ColorCardGroupsController : ControllerBase
    {
        public ColorCardGroupsController()
        {
            AutoMapper.Mapper.CreateMap<ColorCardGroup, ColorCardGroupVm>()
                      .ForMember(m => m.cover_image, x => x.Ignore())
                      .ForMember(m => m.Name, x=>x.MapFrom(y=>y.Translations.Display(SessionHelper.CurrentLanguage).Name))
                      .ForMember(m => m.Language, x => x.MapFrom(y => SessionHelper.CurrentLanguage));
        }
        
        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Create(ColorCardGroupVm group)
        {
            if (string.IsNullOrEmpty(group.Name))
            {
                TempData["Error"] = Views.ColorCards.Error_NameOfGroupIsRequired;
                return View("new", group);
            }

            
            var translations = Conf.LanguageCodes.Select(l => new ColorCardGroupTranslation
                {
                    Name = group.Name, Language = l
                }).ToList();

            var card = new ColorCardGroup{
                    Translations = translations
                };

            var cover = FileUpload(group.cover_image, Conf.ColorCardGroupCoverImagePath,
                                   Conf.ColorCardGroupCoverImageThumbnailSize);
            if (cover.ContainsKey("Image"))
            {
                card.Image = cover["Image"];
                card.ImageThumb = cover["Thumbnail"];
            }

            _db.ColorCardGroups.Add(card);
            _db.SaveChanges();

            return Redirect("~/color-cards");
        }

        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Edit(int id)
        {
            var vm = AutoMapper.Mapper.Map<ColorCardGroup, ColorCardGroupVm>(_db.ColorCardGroups.Find(id));
            if (vm == null) return HttpNotFound();

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Update(ColorCardGroupVm req)
        {
            if (ModelState.IsValid)
            {
                var pData = _db.ColorCardGroups.Find(req.Id);
                if (pData == null) return HttpNotFound();

                pData.Translations.Display(req.Language).Name = req.Name;

                var origImg = Server.MapPath(Conf.ColorCardGroupCoverImagePath + Path.DirectorySeparatorChar + pData.Image);
                var origThumb = Server.MapPath(Conf.ColorCardGroupCoverImagePath + Path.DirectorySeparatorChar + pData.ImageThumb);
                var img = FileUpload(req.cover_image, Conf.ColorCardGroupCoverImagePath, Conf.ColorCardThumbnailSize);

                if (img.ContainsKey("Image"))
                {
                    pData.Image = img["Image"];
                    pData.ImageThumb = img["Thumbnail"];
                }

                if (_db.SaveChanges() > 0)
                {
                    // Delete existing files
                    if (img.ContainsKey("Image"))
                    {
                        if (System.IO.File.Exists(origImg)) System.IO.File.Delete(origImg);
                        if (System.IO.File.Exists(origThumb)) System.IO.File.Delete(origThumb);
                    }
                }

                TempData["Notice"] = Views.ColorCards.Notice_UpdatedSuccessfully;

                return RedirectToAction("edit", new { id = pData.Id });
            }

            return View("edit", req);
        }
    }
}
