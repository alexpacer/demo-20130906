﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Project20130906.App_LocalResources.Views;
using Project20130906.Controllers.Filters;
using Project20130906.Helpers;
using Project20130906.Libs;
using Project20130906.Libs.Entities;
using Project20130906.Models.ViewModels;

namespace Project20130906.Controllers
{
    public class CategoriesController : ControllerBase
    {
        [PermissionFilter(Def.Permissions.Category_Management)]
        public ActionResult Index()
        {
            var categories = _db.Categories;

            return View(categories.ToList());
        }

        [HttpGet]
        [PermissionFilter(Def.Permissions.Category_Management)]
        public ActionResult Edit(int? id)
        {
            Category cat = _db.Categories.Find(id);
            if (cat == null) return HttpNotFound();

            var catVm = AutoMapper.Mapper.Map<Category, CategoryVm>(cat);
            if (cat.ParentId == null)
                return View(catVm);
            else
                return View("EditForSubCategory", catVm);
        }

        [HttpPost]
        [PermissionFilter(Def.Permissions.Category_Management)]
        public ActionResult Edit(CategoryVm request)
        {
            Category cat = _db.Categories.Find(request.Id);
            if (cat == null) return HttpNotFound();

            var errorView = (cat.ParentId == null) ? "edit" : "EditForSubCategory";

            if (string.IsNullOrEmpty(request.Name))
            {
                TempData["Error"] = Categories.Error_CategoryNameIsRequired;
                return View(errorView, request);
            }

            var trans = cat.Translations.FirstOrDefault(x => x.Language == request.Language);
            if (trans == null)
            {
                trans = new CategoryTranslation
                    {
                        CategoryId = request.Id,
                        Language = request.Language,
                        Name = request.Name,
                        Slogan = request.Slogan,
                        Description = request.Description
                    };
                cat.Translations.Add(trans);
            }
            else
            {
                trans.Name = request.Name;
                trans.Slogan = request.Slogan;
                trans.Description = request.Description;
            }

            #region Background Image
            var origBg = Server.MapPath(Conf.CategoryBackgroundPath + Path.DirectorySeparatorChar + cat.BackgroundImage);
            var origBgThumb = Server.MapPath(Conf.CategoryBackgroundPath + Path.DirectorySeparatorChar + cat.BackgroundImageThumb);
            Dictionary<string, string> bg = new Dictionary<string, string>();
            try
            {
                bg = FileUpload(request.background_image, Conf.CategoryBackgroundPath,
                                    Conf.CategoryBackgroundThumbnailSize);
                if (bg.ContainsKey("Image"))
                {
                    cat.BackgroundImage = bg["Image"];
                    cat.BackgroundImageThumb = bg["Thumbnail"];
                }
            }
            catch (ArgumentException)
            {
                TempData["Error"] = Common.Error_ImageFileIsInWrongFormat;
                return View(errorView, request);
            }
            #endregion

            #region Cover Image
            var origCover = Server.MapPath(Conf.CategoryBackgroundPath + Path.DirectorySeparatorChar + cat.CoverImage);
            var origCoverThumb = Server.MapPath(Conf.CategoryBackgroundPath + Path.DirectorySeparatorChar + cat.CoverImageThumb);
            Dictionary<string, string> cover = new Dictionary<string, string>();
            try
            {
                cover = FileUpload(request.cover_image, Conf.CategoryCoverImagePath, Conf.CategoryCoverThumbnailSize);
                if (cover.ContainsKey("Image"))
                {
                    cat.CoverImage = cover["Image"];
                    cat.CoverImageThumb = cover["Thumbnail"];
                }
            }
            catch (ArgumentException)
            {
                TempData["Error"] = Common.Error_ImageFileIsInWrongFormat;
                return View(errorView, request);
            }
            #endregion

            #region Slogan Image
            var sloganCover = Server.MapPath(Conf.CategorySloganImagePath + Path.DirectorySeparatorChar + cat.SloganImage);
            var sloganThumb = Server.MapPath(Conf.CategorySloganImagePath + Path.DirectorySeparatorChar + cat.SloganImageThumb);
            Dictionary<string, string> slogan = new Dictionary<string, string>();
            try
            {
                slogan = FileUpload(request.slogan_image, Conf.CategorySloganImagePath, Conf.CategorySloganImageThumbSize);
                if (slogan.ContainsKey("Image"))
                {
                    cat.SloganImage = slogan["Image"];
                    cat.SloganImageThumb = slogan["Thumbnail"];
                }
            }
            catch (ArgumentException)
            {
                TempData["Error"] = Common.Error_ImageFileIsInWrongFormat;
                return View(errorView, request);
            }
            #endregion

            cat.UpdatedAt = DateTime.UtcNow;
            cat.UpdatedBy = User.Identity.Name;

            _db.SaveChanges();

            // Delete existing files
            if (bg.ContainsKey("Image"))
            {
                if (System.IO.File.Exists(origBg)) System.IO.File.Delete(origBg);
                if (System.IO.File.Exists(origBgThumb)) System.IO.File.Delete(origBgThumb);
            }

            if (cover.ContainsKey("Image"))
            {
                if (System.IO.File.Exists(origCover)) System.IO.File.Delete(origCover);
                if (System.IO.File.Exists(origCoverThumb)) System.IO.File.Delete(origCoverThumb);
            }

            if (slogan.ContainsKey("Image"))
            {
                if (System.IO.File.Exists(sloganCover)) System.IO.File.Delete(sloganCover);
                if (System.IO.File.Exists(sloganThumb)) System.IO.File.Delete(sloganThumb);
            }

            TempData["Notice"] = Categories.Note_CategoryUpdatedSuccessfully;
            return RedirectToAction("edit", "categories", new { id = request.Id });
        }

        [HttpGet]
        [PermissionFilter(Def.Permissions.Category_Management)]
        public ActionResult New(int? parent_id)
        {
            Category parent = _db.Categories.FirstOrDefault(p => p.Id == parent_id);
            ViewBag.ParentCategory = parent;
            if (parent == null)
                return View(new CategoryVm { ParentId = parent_id });
            else
                return View("NewForSubCategory", new CategoryVm { ParentId = parent_id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Category_Management)]
        public ActionResult Create(CategoryVm request)
        {
            Category parent = _db.Categories.FirstOrDefault(x => x.Id == request.ParentId);
            ViewBag.ParentCategory = parent;
            var errorView = (request.ParentId == null) ? "new" : "NewForSubCategory";

            #region Validations
            if (string.IsNullOrEmpty(request.Name))
            {
                TempData["Error"] = Categories.Error_CategoryNameIsRequired;
                return View(errorView, request);
            }

            if (request.Description.Length > Conf.CategoryDescriptionMaxLength)
            {
                TempData["Error"] = Categories.Error_CategoryDescriptionIsTooLong;
                return View(errorView, request);
            }
            #endregion


            var translations = new List<CategoryTranslation>();
            foreach (var l in Conf.LanguageCodes)
            {
                var trans = new CategoryTranslation
                    {
                        Name = request.Name,
                        Slogan = request.Slogan,
                        Description = request.Description,
                        Language = l
                    };
                translations.Add(trans);
            }

            var cNew = new Category
                {
                    Order = 0,
                    Parent = parent ?? null,
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = HttpContext.User.Identity.Name,
                    Translations = translations
                };

            var bg = FileUpload(request.background_image, Conf.CategoryBackgroundPath, Conf.CategoryBackgroundThumbnailSize);
            var cover = FileUpload(request.cover_image, Conf.CategoryCoverImagePath, Conf.CategoryCoverThumbnailSize);
            var slogan = FileUpload(request.slogan_image, Conf.CategorySloganImagePath, Conf.CategorySloganImageThumbSize);

            if (bg.ContainsKey("Image"))
            {
                cNew.BackgroundImage = bg["Image"];
                cNew.BackgroundImageThumb = bg["Thumbnail"];
            }

            if (cover.ContainsKey("Image"))
            {
                cNew.CoverImage = cover["Image"];
                cNew.CoverImageThumb = cover["Thumbnail"];
            }

            if (slogan.ContainsKey("Image"))
            {
                cNew.SloganImage = slogan["Image"];
                cNew.SloganImageThumb = slogan["Thumbnail"];
            }

            _db.Categories.Add(cNew);
            _db.SaveChanges();

            return Redirect("index");
        }

    }
}
