﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Project20130906.App_LocalResources.Views;
using Project20130906.Controllers.Filters;
using Project20130906.Helpers;
using Project20130906.Libs;
using Project20130906.Libs.Entities;
using Project20130906.Models.ViewModels;

namespace Project20130906.Controllers
{
    public class NewsController : ControllerBase
    {
        [HttpGet]
        [PermissionFilter(Def.Permissions.News_Management)]
        public ActionResult Index(int page = 1)
        {
            var user = _db.Users.FirstOrDefault(x => x.Id.EndsWith(User.Identity.Name));
            if (!user.Permissions.Any(
                    x => x.Id == (int) Def.Permissions.News_Management || x.Id == (int) Def.Permissions.News_Amendment))
            {
                return RedirectToAction("home");
            }

            var pageIndex = ((page - 1) <= 0) ? 0 : page - 1;
            var pageSize = SessionHelper.PageSize;
            var total = _db.NewsPosts.Count();
            var pageCount = (int)Math.Ceiling((double)total / pageSize);
            var news = _db.NewsPosts
                          .OrderByDescending(x => (x.ModifiedAt.HasValue) ? x.ModifiedAt.Value : x.CreateAt)
                          .Skip(pageIndex * pageSize)
                          .Take(pageSize);
            var vm = new NewsIndexVm
                {
                    NewsPosts = Mapper.Map<IEnumerable<NewsPost>, IEnumerable<NewsPostVm>>(news),
                    Paging = new PagingVm(total, pageCount, page)
                };

            return View(vm);
        }

        [HttpGet]
        public ActionResult View(int id)
        {
            var news = _db.NewsPosts.FirstOrDefault(x => x.Id == id);
            if (news == null) return HttpNotFound();

            var user = _db.Users.FirstOrDefault(x => x.Id.EndsWith(User.Identity.Name));
            ViewBag.HasEditPermission = user.Permissions.Any(x=>x.Id == (int)Def.Permissions.News_Management || x.Id == (int)Def.Permissions.News_Amendment);

            return View(news);
        }

        [HttpGet]
        public ActionResult Home()
        {
            var pageSize = SessionHelper.PageSize;
            var total = _db.NewsPosts.Count();
            var news = _db.NewsPosts
                          .OrderByDescending(x => (x.ModifiedAt.HasValue) ? x.ModifiedAt.Value : x.CreateAt)
                          .Skip(0 * pageSize)
                          .Take(pageSize);

            return View(news.AsEnumerable());
        }

        [HttpDelete]
        [PermissionFilter(Def.Permissions.News_Amendment)]
        public void Delete(int id)
        {

        }

        [HttpGet]
        [PermissionFilter(Def.Permissions.News_Management)]
        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        [PermissionFilter(Def.Permissions.News_Management)]
        public ActionResult Create(NewsPostVm request)
        {
            var translations = new List<NewsPostTranslation>();
            foreach (var l in Conf.LanguageCodes)
            {
                var trans = new NewsPostTranslation
                {
                    Description = request.Translation.Description,
                    Subject = request.Translation.Subject,
                    Language = l
                };
                translations.Add(trans);
            }

            var post = new NewsPost
                {
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = User.Identity.Name,
                    Translations = translations
                };

            _db.NewsPosts.Add(post);
            if (_db.SaveChanges() > 0)
            {
                TempData["Notice"] = News.Notice_NewsPostCreatedSuccessfully;
            }

            return RedirectToAction("index");
        }

        [HttpGet]
        [PermissionFilter(Def.Permissions.News_Management)]
        public ActionResult Edit(int id)
        {
            var newsPost = _db.NewsPosts.FirstOrDefault(x => x.Id == id);
            if (newsPost == null) return RedirectToAction("index");

            var vm = AutoMapper.Mapper.Map<NewsPost, NewsPostVm>(newsPost);
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.News_Management)]
        public ActionResult Update(NewsPostVm request)
        {
            var language = SessionHelper.CurrentLanguage;
            var post = _db.NewsPosts.FirstOrDefault(x => x.Id == request.Id);
            if (post == null) return RedirectToAction("index");
            var translation = post.Translations.FirstOrDefault(x => x.Language == language);
            if (translation == null) return RedirectToAction("index");

            translation.Description = request.Translation.Description;
            translation.Subject = request.Translation.Subject;
            post.ModifiedAt = DateTime.UtcNow;
            post.ModifiedBy = User.Identity.Name;

            if (_db.SaveChanges() > 0)
            {
                TempData["Notice"] = News.Notice_NewsPostUpdatedSuccessfully;
            }

            return RedirectToAction("index");
        }
    }
}
