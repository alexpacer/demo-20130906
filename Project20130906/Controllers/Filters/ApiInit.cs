﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Project20130906.Helpers;

namespace Project20130906.Controllers.Filters
{
    public class ApiInit : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext context)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(SessionHelper.CurrentLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(SessionHelper.CurrentLanguage);

            var controller = (WebApiController)context.ControllerContext.Controller;

            base.OnActionExecuting(context);
        }
    }


}