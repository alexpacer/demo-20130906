﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
using Project20130906.Helpers;

namespace Project20130906.Controllers.Filters
{

    public class ControllerInit : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(SessionHelper.CurrentLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(SessionHelper.CurrentLanguage);

            var controller = (ControllerBase)filterContext.Controller;

            try
            {
                controller.Initialize();
                base.OnActionExecuting(filterContext);
            }
            catch (System.InvalidOperationException ex)
            {
                var cName = filterContext.Controller.ControllerContext.RouteData.Values["controller"];
                var cAtion = filterContext.Controller.ControllerContext.RouteData.Values["action"];
                if (ex.Source == "EntityFramework" &&
                    (cName.ToString() != "home" && cAtion.ToString() != "install"))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                        (
                            new
                                {
                                    action = "install", 
                                    controller = "setup",
                                    area = ""
                                }
                        ));
                    return;
                }

            }
        }

        
    }
}