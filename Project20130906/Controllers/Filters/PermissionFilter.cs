﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Project20130906.Helpers;
using Project20130906.Libs;
using Project20130906.Libs.Context;
using Project20130906.Libs.Entities;

namespace Project20130906.Controllers.Filters
{
    public class PermissionFilter : ActionFilterAttribute
    {
        private int _permissionId;

        public PermissionFilter(Def.Permissions permissionId)
        {
            _permissionId = (int)permissionId;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (var db = new ModelContext())
            {
                User usr;
                int[] permissions;
                if (SessionHelper.UserPermissions == null || SessionHelper.UserPermissions.Length == 0)
                {
                    var identity = filterContext.HttpContext.User.Identity;
                    usr = db.Users.FirstOrDefault(x => x.Id == identity.Name);
                    if (usr == null)
                    {
                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary(new
                                {
                                    action = "home",
                                    controller = "news",
                                    area = ""
                                }));
                        return;
                    }
                    SessionHelper.UserPermissions = usr.Permissions.Select(x => x.Id).ToArray();
                }
                
                if (!SessionHelper.UserPermissions.Contains(_permissionId))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                        new { action = "home", controller = "news", area = "" }));
                    return;
                }
            }
            
            base.OnActionExecuting(filterContext);
        }

    }
}