﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Project20130906.Libs;
using Project20130906.Libs.Entities;

namespace Project20130906.Controllers
{
    public class PermissionsController : ControllerBase
    {
        //
        // GET: /Permissions/

        public ActionResult Index()
        {
            var permissions = _db.Permissions.ToList();
            return View(permissions);
        }

        public ActionResult Edit(int id)
        {
            PrincipalContext principal;
            if (Conf.UserLookupMode == "domain")
            {
                principal = new PrincipalContext(ContextType.Domain);
            }
            else
            {
                principal = new PrincipalContext(ContextType.Machine);
            }

            var users = new List<User>();
            var search = new PrincipalSearcher(new UserPrincipal(principal));
            foreach (var userPrincipal in search.FindAll())
            {
                if (userPrincipal != null && userPrincipal.DisplayName != null)
                {
                    var user = new User() { Id = userPrincipal.SamAccountName, Email = userPrincipal.DisplayName };
                    users.Add(user);
                }
            }
            ViewBag.DomainUsers = users;



            var permission = _db.Permissions.FirstOrDefault(x => x.Id == id);

            if (permission == null)
                return RedirectToAction("index");

            return View(permission);
        }

    }
}
