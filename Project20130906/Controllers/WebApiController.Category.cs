﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project20130906.App_LocalResources.Views;
using Project20130906.Helpers;
using Project20130906.Libs;
using Project20130906.Libs.Entities;
using Project20130906.Models.Api;
using Project20130906.Models.ViewModels;

namespace Project20130906.Controllers
{
    public partial class WebApiController
    {
        [HttpPost]
        public ApiResponse UpdateCategoryOrder(HttpRequestMessage request)
        {
            var reqStr = request.Content.ReadAsStringAsync().Result;
            IEnumerable<CategoryOrderRequest> orders = JsonConvert.DeserializeObject<IList<CategoryOrderRequest>>(reqStr);
            var response = new ApiResponse();

            foreach (var req in orders)
            {
                var item = _db.Categories.FirstOrDefault(x => x.Id == req.id);
                if (item != null)
                {
                    item.Order = req.order;
                }
            }
            _db.SaveChanges();
            response.message = Categories.Notice_CategoryOrderUpdatedSuccessfully;
            return response;
        }

        [HttpPut]
        public ApiResponse UpdateCategory(int id, CategoryRequest request)
        {
            var errorResponse = new ApiResponse { status = "error" };
            var persisCategory = _db.Categories.FirstOrDefault(x => x.Id == request.id);
            if (persisCategory == null)
            {
                errorResponse.message = Categories.Error_CategoryDoesNotExists;
                return errorResponse;
            }


            var i18n = persisCategory.Translations.FirstOrDefault(x => x.Language == request.language);
            if (i18n == null)
            {
                i18n = new CategoryTranslation
                {
                    Language = request.language,
                    CategoryId = persisCategory.Id,
                    Name = request.name
                };
                persisCategory.Translations.Add(i18n);
            }
            else
            {
                i18n.Name = request.name;
            }
            persisCategory.BackgroundImage = request.background_image_file;
            persisCategory.BackgroundImageThumb = request.background_image_thumb_file;
            persisCategory.Order = request.order;
            persisCategory.UpdatedAt = DateTime.UtcNow;
            persisCategory.UpdatedBy = User.Identity.Name;

            _db.SaveChanges();

            // delete unwanted uploaded images
            foreach (var x in request.uploaded_images.Split(',').Where(i => i != persisCategory.BackgroundImage))
            {
                if (!string.IsNullOrWhiteSpace(x))
                {
                    var path = HttpContext.Current.Server.MapPath(Conf.CategoryBackgroundPath + "/" + x);
                    if (File.Exists(path)) { File.Delete(path); }

                    path = HttpContext.Current.Server.MapPath(Conf.CategoryBackgroundPath + "/" + x.Split('.').First() + "_thumb." + x.Split('.').Last());
                    if (File.Exists(path)) { File.Delete(path); }
                }
            }


            return new ApiResponse() { message = "success" };
        }

        [HttpDelete]
        public ApiResponse DeleteCategory(int id)
        {
            var category = _db.Categories.FirstOrDefault(x => x.Id == id);

            if (category == null)
            {
                return new ApiResponse() { status = "error", message = Categories.Error_CategoryDoesNotExists };
            }

            _db.Categories.Remove(category);
            _db.SaveChanges();

            return new ApiResponse() { status = "success", message = Categories.Notice_CategoryDeletedSuccessfully };
        }

        [HttpPost]
        public Task<object> UploadCategoryImage(int category_id)
        {
            HttpRequestMessage request = this.Request;
            if (!request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath(Conf.CategoryBackgroundPath);
            var provider = new MultipartFormDataStreamProvider(root);

            var task = request.Content.ReadAsMultipartAsync(provider)
                .ContinueWith<object>(o =>
                {
                    var category = _db.Categories.FirstOrDefault(x => x.Id == category_id);
                    if (category == null) return new JObject { { "status", "error" }, { "message", Categories.Error_CategoryDoesNotExists } };

                    var serverPath = Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, "");
                    FileInfo finfo = new FileInfo(provider.FileData.First().LocalFileName);
                    var fileType = Utils.GetMimeType(finfo);
                    long fileSize = finfo.Length;
                    var guid = Guid.NewGuid().ToString().Replace("-", "");

                    var origFileName = System.IO.Path.GetFileName(provider.FileData.First()
                                               .Headers.ContentDisposition.FileName.Replace("\"", ""));
                    if (origFileName.Length > 25)
                    {
                        origFileName = origFileName.Split('.').First().Substring(0, 19) + "." + origFileName.Split('.').Last();
                    }

                    var targetFileName = string.Format(@"{0}_{1}", guid, origFileName);
                    var targetThumbFileName = string.Format(@"{0}_{1}_thumb.{2}", guid, origFileName.Split('.').First(), origFileName.Split('.').Last());

                    File.Move(finfo.FullName,
                        Path.Combine(root, targetFileName));

                    // create thumb
                    using (var img = Image.FromFile(Path.Combine(root, targetFileName)))
                    {
                        var newWidth = Conf.CategoryCoverThumbnailSize;
                        var aa = (decimal)img.Height / (decimal)img.Width * newWidth;
                        var newHeight = (int)Decimal.Round(aa, 0, MidpointRounding.AwayFromZero);

                        using (Image thumb = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero))
                        {
                            thumb.Save(Path.Combine(root, targetThumbFileName));
                        }
                    }

                    return new JObject
                        {
                            {"files", new JArray{
                                new JObject{
                                { "url", 
                                    serverPath + 
                                    VirtualPathUtility.ToAbsolute(Conf.CategoryBackgroundPath + "/" + targetFileName) },
                                { "thumbnail_url", 
                                    serverPath + 
                                    VirtualPathUtility.ToAbsolute(Conf.CategoryBackgroundPath + "/" + targetThumbFileName)},
                                { "name", targetFileName },
                                { "name_thumb", targetThumbFileName },
                                { "type", fileType },
                                { "size", fileSize },
                                { "delete_url", Url.Route("api-delete-image-category", new { targetFileName }) },
                                { "delete_type","DELETE"}
                                }
                            }}
                        };
                }
            );
            return task;
        }

        [HttpDelete]
        public ApiResponse DeleteCategoryImageFile(string file)
        {
            FileInfo fInfo = new FileInfo(HostingEnvironment.MapPath(Conf.CategoryBackgroundPath + "/" + file));

            if (fInfo.Exists)
            {
                fInfo.Delete();
            }

            return new ApiResponse();
        }


       

    }
}