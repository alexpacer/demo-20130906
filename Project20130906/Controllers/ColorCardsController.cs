﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project20130906.App_LocalResources.Views;
using Project20130906.Controllers.Filters;
using Project20130906.Helpers;
using Project20130906.Libs;
using Project20130906.Libs.Entities;
using Project20130906.Models.ViewModels;

namespace Project20130906.Controllers
{
    public class ColorCardsController : ControllerBase
    {
        public ColorCardsController()
        {
            AutoMapper.Mapper.CreateMap<ColorCardGroup, ColorCardGroupVm>();
            AutoMapper.Mapper.CreateMap<ColorCard, ColorCardVm>()
                .ForMember(m => m.submit_image, x => x.Ignore())
                .ForMember(m => m.group, x=>x.Ignore())
                .ForMember(m => m.CurrentGroup, x=>x.MapFrom(y=>y.Group));
        }

        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Index()
        {
            var colorcards = _db.ColorCards.Include(c => c.Creator);
            var emptyGroups = _db.ColorCardGroups.Where(g => g.ColorCards.Count == 0);
            return View(new ColorCardPageVm{ 
                ColorCards = colorcards.ToList(),
                EmptyGroups = emptyGroups.ToList()
            });
        }

        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Details(int id = 0)
        {
            ColorCard colorcard = _db.ColorCards.Find(id);
            if (colorcard == null)
            {
                return HttpNotFound();
            }
            return View(colorcard);
        }

        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult New()
        {
            ViewBag.AllGroups = _db.ColorCardGroups.ToList();
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Create(ColorCardVm colorcard)
        {
            if (colorcard.submit_image == null || colorcard.submit_image.ContentLength == 0 || !colorcard.submit_image.IsImage())
            {
                TempData["Error"] = @"Color card image is required.";
                return View("new", colorcard);
            }

            if (string.IsNullOrWhiteSpace(colorcard.Name)){
                TempData["Error"] = @"Name of the Color Card is required.";
                return View("new", colorcard);
            }

            var img = FileUpload(colorcard.submit_image, Conf.ColorCardImagePath, Conf.ColorCardThumbnailSize);
            var card = new ColorCard()
                {
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = _currentUser.Id,
                    Name = colorcard.Name
                };
            if (img.ContainsKey("Image"))
            {
                card.Image = img["Image"];
                card.ImageThumb = img["Thumbnail"];
            }

            _db.ColorCards.Add(card);
            _db.SaveChanges();

            TempData["Notice"] = "Color card created successfully.";


            return RedirectToActionPermanent("index");
        }

        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Edit(int id = 0)
        {
            ColorCard colorcard = _db.ColorCards.Find(id);
            if (colorcard == null){ return HttpNotFound(); }

            var vm = AutoMapper.Mapper.Map<ColorCard, ColorCardVm>(colorcard);

            ViewBag.AllGroups = _db.ColorCardGroups.ToList();

            return View(vm);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Update(ColorCardVm colorcard)
        {
            if (ModelState.IsValid)
            {
                var ccard = _db.ColorCards.FirstOrDefault(x => x.Id == colorcard.Id);
                if (ccard == null) return HttpNotFound();

                ccard.Name = colorcard.Name;
                ccard.GroupId = colorcard.group;

                var origImg = Server.MapPath(Conf.ColorCardImagePath + Path.DirectorySeparatorChar + ccard.Image);
                var origThumb = Server.MapPath(Conf.ColorCardImagePath + Path.DirectorySeparatorChar + ccard.ImageThumb);
                var img = FileUpload(colorcard.submit_image, Conf.ColorCardImagePath, Conf.ColorCardThumbnailSize);

                if (img.ContainsKey("Image"))
                {
                    ccard.Image = img["Image"];
                    ccard.ImageThumb = img["Thumbnail"];
                }

                ccard.UpdateAt = DateTime.UtcNow;
                ccard.UpdateBy = User.Identity.Name;

                if (_db.SaveChanges() > 0)
                {
                    // Delete existing files
                    if (img.ContainsKey("Image"))
                    {
                        if (System.IO.File.Exists(origImg)) System.IO.File.Delete(origImg);
                        if (System.IO.File.Exists(origThumb)) System.IO.File.Delete(origThumb);
                    }
                }

                TempData["Notice"] = ColorCards.Notice_UpdatedSuccessfully;

                return RedirectToAction("edit", new {id = colorcard.Id});
            }
            
            return View("edit", colorcard);
        }


        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult Delete(int id = 0)
        {
            ColorCard colorcard = _db.ColorCards.Find(id);
            if (colorcard == null)
            {
                return HttpNotFound();
            }

            return View(colorcard);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Color_Card_Management)]
        public ActionResult DeleteConfirmed(int id)
        {
            ColorCard colorcard = _db.ColorCards.Find(id);
            if (colorcard == null) return HttpNotFound();

            var cardName = colorcard.Name;
            var origImg = Server.MapPath(Conf.ColorCardImagePath + Path.DirectorySeparatorChar + colorcard.Image);
            var origThumb = Server.MapPath(Conf.ColorCardImagePath + Path.DirectorySeparatorChar + colorcard.Image);

            _db.ColorCards.Remove(colorcard);
            _db.SaveChanges();

            if (_db.SaveChanges() > 0)
            {
                if (System.IO.File.Exists(origImg)) System.IO.File.Delete(origImg);
                if (System.IO.File.Exists(origThumb)) System.IO.File.Delete(origThumb);
            }

            TempData["Notice"] = string.Format(ColorCards.Notice_DeleteSuccessfully, cardName);

            return RedirectToAction("index");
        }


    }
}