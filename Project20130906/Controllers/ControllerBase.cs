﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hammock;
using Hammock.Web;

namespace Project20130906.Controllers
{
    using Project20130906.Controllers.Filters;
    using Project20130906.Helpers;
    using Project20130906.Libs;
    using Project20130906.Libs.Context;
    using Project20130906.Libs.Entities;
    using Project20130906.Models.ViewModels;

    [ControllerInit]
    public class ControllerBase : Controller
    {
        protected ModelContext _db = new ModelContext();
        protected User _currentUser;
        protected string _currentLanguage;
        private Guid xAccessToken = new Guid("AC2983FD-C965-4013-A31D-19C13528F1F0");
        private string xProjectKey = "";

        /// <summary>
        /// Controller entry point
        /// </summary>
        public void Initialize()
        {
            var userid = User.Identity.Name;

            _currentUser = _db.Users.FirstOrDefault(x => x.Id == userid);
            ViewBag.User = _currentUser;
            if (_currentUser != null)
            {
                ViewBag.MenuItems = _db.Permissions
                                       .Where(p => p.IsMenu && p.Users
                                                                .Any(u => u.Id == userid)).ToList();
                SessionHelper.UserPermissions = _currentUser.Permissions.Select(x => x.Id).ToArray();
            }
            else
            {
                _db.Users.Add(new User
                    {
                        Id = userid
                    });
                _db.SaveChanges();
                _currentUser = _db.Users.FirstOrDefault(x => x.Id == userid);
            }

            if (Conf.DisplayAllMenus)
            {
                ViewBag.MenuItems = _db.Permissions.Where(p => p.IsMenu).ToList();
            }

            // License Check


            var request = new RestRequest
                {
                    Path = "http://localhost"
                };

            // Auto Mappers --------------------

            AutoMapper.Mapper.CreateMap<NewsPost, NewsPostVm>()
                      .ForMember(x => x.Translation, y => y.MapFrom(oo =>
                            new NewsPostVmTranslation
                            {
                                Subject = oo.GetTranslation(SessionHelper.CurrentLanguage).Subject,
                                Description = oo.GetTranslation(SessionHelper.CurrentLanguage).Description
                            })
                          );

            AutoMapper.Mapper.CreateMap<Category, CategoryVm>()
                      .ForMember(c => c.Name,
                                 m =>
                                 m.MapFrom(o => o.GetTranslation(SessionHelper.CurrentLanguage).Name))
                      .ForMember(c => c.Slogan,
                                 m =>
                                 m.MapFrom(o => o.GetTranslation(SessionHelper.CurrentLanguage).Slogan))
                      .ForMember(c => c.Description,
                                 m =>
                                 m.MapFrom(o => o.GetTranslation(SessionHelper.CurrentLanguage).Description))
                      .ForMember(c => c.background_image, m => m.Ignore())
                      .ForMember(c => c.cover_image, m => m.Ignore())
                      .ForMember(c => c.slogan_image, m => m.Ignore());

            AutoMapper.Mapper.CreateMap<ColorCard, ColorCardVm>()
                      .ForMember(c => c.submit_image,
                                 m => m.Ignore());

            AutoMapper.Mapper.CreateMap<Product, ProductVm>()
                .ForMember(c => c.Name, m => m.MapFrom(x => x.GetTranslation(SessionHelper.CurrentLanguage).Name))
                .ForMember(c => c.ShortDescription, m => m.MapFrom(o => o.GetTranslation(SessionHelper.CurrentLanguage).ShortDescription))
                .ForMember(c => c.FullDescription, m => m.MapFrom(o => o.GetTranslation(SessionHelper.CurrentLanguage).FullDescription))
                .ForMember(c => c.Remark, m => m.MapFrom(o => o.GetTranslation(SessionHelper.CurrentLanguage).Remark))
                     .ForMember(c => c.CurrentCategories, m => m.MapFrom(o => AutoMapper.Mapper.Map<ICollection<Category>, ICollection<CategoryVm>>(o.Categories)))
                     .ForMember(c => c.CurrentColorCards, m => m.MapFrom(o => AutoMapper.Mapper.Map<ICollection<ColorCard>, ICollection<ColorCardVm>>(o.ColorCards)))
                     .ForMember(c => c.AllColorCards, m => m.Ignore())
                     .ForMember(c => c.AllCategories, m => m.Ignore())
                     .ForMember(x => x.language, m => m.MapFrom(y => y.Translations.Where(ot => ot.Language == SessionHelper.CurrentLanguage).Select(ots => ots.Language).FirstOrDefault()))
                     .ForMember(c => c.submit_image, m => m.Ignore())
                     .ForMember(c => c.color_cards, m => m.Ignore())
                     .ForMember(c => c.categories, m => m.MapFrom(o => o.Categories.Select(i => i.Id).ToList()));

        }

        protected Dictionary<string, string> FileUpload(HttpPostedFileBase file, string targetFolder, int resizeWidth)
        {
            var ret = new Dictionary<string, string>();

            if (file != null && file.ContentLength > 0 && file.IsImage())
            {
                var fileName = Path.GetFileName(file.FileName);

                using (var img = Image.FromStream(file.InputStream))
                {
                    //original height / original width x new width = new height
                    var aa = (decimal)img.Height / (decimal)img.Width * resizeWidth;
                    var newHeight = (int)Decimal.Round(aa, 0, MidpointRounding.AwayFromZero);

                    var newFileName = string.Format(@"{0}-{1}{2}", Guid.NewGuid().ToString().Replace("-", ""),
                                                    Utils.DateTimeToPosixTimestamp(DateTime.Now),
                                                    Path.GetExtension(fileName));
                    var newThumbName = string.Format(@"{0}_thumb{1}", newFileName, Path.GetExtension(fileName));

                    img.Save(Server.MapPath(targetFolder + Path.DirectorySeparatorChar + newFileName));

                    using (var thumb = img.GetThumbnailImage(resizeWidth, newHeight, () => false, IntPtr.Zero))
                    {
                        thumb.Save(Server.MapPath(targetFolder + Path.DirectorySeparatorChar + newThumbName));
                    }

                    ret.Add("Image", newFileName);
                    ret.Add("Thumbnail", newThumbName);
                }

            }
            return ret;
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

    }
}