﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Objects.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Project20130906.App_LocalResources.Views;
using Project20130906.Controllers.Filters;
using Project20130906.Helpers;
using Project20130906.Libs;
using Project20130906.Libs.Entities;
using Project20130906.Models.ViewModels;

namespace Project20130906.Controllers
{
    public class ProductsController : ControllerBase
    {
        public ProductsController()
        {
            AutoMapper.Mapper.CreateMap<ColorCardGroup, ColorCardGroupVm>();
            AutoMapper.Mapper.CreateMap<ColorCard, ColorCardVm>()
                .ForMember(m => m.submit_image, x => x.Ignore())
                .ForMember(m => m.group, x => x.Ignore())
                .ForMember(m => m.CurrentGroup, x => x.MapFrom(y => y.Group));    
        }

        [PermissionFilter(Def.Permissions.Product_Management)]
        public ActionResult Index()
        {
            var allCategories = _db.Categories.AsEnumerable();
            var keyword = Request.Params["keyword"];
            int cat;
            Category category = null;
            if (Int32.TryParse(Request.Params["c"], out cat))
                category = _db.Categories.FirstOrDefault(x => x.Id == cat);

            IEnumerable<Product> products = _db.Products;

            if (category != null)
                products = products.Where(x => x.Categories.Any(c => c.Id == category.Id));

            if (!string.IsNullOrEmpty(keyword))
                products = _db.Products.AsQueryable()
                .Where(p =>
                     p.Translations.Any(tt =>
                            (!string.IsNullOrEmpty(tt.Name) && tt.Name.Contains(keyword)) ||
                            (!string.IsNullOrEmpty(tt.Remark) && tt.Remark.Contains(keyword)) ||
                            (!string.IsNullOrEmpty(tt.ShortDescription) && tt.ShortDescription.Contains(keyword)) ||
                            (!string.IsNullOrEmpty(tt.FullDescription) && tt.FullDescription.Contains(keyword))));

            TempData["keyword"] = keyword;
            TempData["category"] = cat.ToString(CultureInfo.InvariantCulture);

            ViewBag.Categories = allCategories.ToList();

            var vms = AutoMapper.Mapper.Map<IEnumerable<Product>, IEnumerable<ProductVm>>(products);
            return View(vms.ToList());
        }

        [PermissionFilter(Def.Permissions.Product_Management)]
        public ActionResult New()
        {
            // fields to choose
            var vm = new ProductVm
                {
                    AllColorCards = _db.ColorCards.ToList(),
                    AllCategories = _db.Categories.ToList()
                };
            return View(vm);
        }

        //
        // POST: /Products/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Product_Management)]
        public ActionResult Create(ProductVm product)
        {
            product.AllCategories = _db.Categories.ToList();
            product.AllColorCards = _db.ColorCards.ToList();

            if (product.ShortDescription.Length > 255)
            {
                TempData["Error"] = Products.Error_DescriptionOver255Chars;
                return View("new", product);
            }

            if (string.IsNullOrEmpty(product.Name))
            {
                TempData["Error"] = Products.Error_ProductNameIsRequired;
                return View("new", product);
            }

            var translations = new List<ProductTranslation>();
            foreach (var l in Conf.LanguageCodes)
            {
                var trans = new ProductTranslation
                {
                    FullDescription = product.FullDescription,
                    ShortDescription = product.ShortDescription,
                    Remark = product.Remark,
                    Name = product.Name,
                    Language = l
                };

                translations.Add(trans);
            }


            var prod = new Product
                {
                    Categories = _db.Categories.Where(x => product.categories.Contains(x.Id)).ToList(),
                    ColorCards = _db.ColorCards.Where(x => product.color_cards.Contains(x.Id)).ToList(),
                    CreateAt = DateTime.UtcNow,
                    CreatedBy = _currentUser.Id,
                    Translations = translations
                };

            if (product.submit_image != null && product.submit_image.ContentLength > 0)
            {
                if (product.submit_image.IsImage())
                {
                    var img = FileUpload(product.submit_image, Conf.ProductImagePath, Conf.ProductImageThumbnailSize);
                    if (img.ContainsKey("Image"))
                    {
                        prod.Image = img["Image"];
                        prod.ImageThumb = img["Thumbnail"];
                    }
                }
            }
            _db.Products.Add(prod);
            _db.SaveChanges();

            return RedirectToActionPermanent("index");
        }

        [PermissionFilter(Def.Permissions.Product_Management)]
        public ActionResult Edit(int id = 0)
        {
            Product product = _db.Products.Find(id);
            if (product == null) { return HttpNotFound(); }

            var vm = AutoMapper.Mapper.Map<Product, ProductVm>(product);

            // fields to choose
            vm.AllColorCards = _db.ColorCards.ToList();
            vm.AllCategories = _db.Categories.ToList();


            return View(vm);
        }

        //
        // POST: /Products/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionFilter(Def.Permissions.Product_Management)]
        public ActionResult Update(ProductVm product)
        {
            var prod = _db.Products.FirstOrDefault(x => x.Id == product.Id);
            if (prod == null) { return HttpNotFound(); }
            var trans = prod.Translations.FirstOrDefault(t => t.Language == product.language);

            product.AllCategories = _db.Categories.ToList();
            product.AllColorCards = _db.ColorCards.ToList();

            var origImg = Server.MapPath(Conf.ProductImagePath + Path.DirectorySeparatorChar + prod.Image);
            var origThumb = Server.MapPath(Conf.ProductImagePath + Path.DirectorySeparatorChar + prod.ImageThumb);

            if (string.IsNullOrEmpty(product.Name))
            {
                TempData["Error"] = Products.Error_ProductNameIsRequired;
                return View("new", product);
            }

            // not in submitted
            var existingProdCats = product.AllCategories.Where(x => prod.Categories.Any(y => y.Id == x.Id));
            foreach (var c in existingProdCats.Where(x => !product.categories.Contains(x.Id)))
            {
                prod.Categories.Remove(c);
            }

            foreach (var c in product.AllCategories.Where(bb => product.categories.Contains(bb.Id) &&
                !existingProdCats.Any(cc => cc.Id == bb.Id)))
            {
                prod.Categories.Add(c);
            }

            // not in submitted
            var existingProdCCards = product.AllColorCards.Where(x => prod.ColorCards.Any(y => y.Id == x.Id));
            foreach (var c in existingProdCCards.Where(x => !product.color_cards.Contains(x.Id)))
            {
                prod.ColorCards.Remove(c);
            }

            foreach (var c in product.AllColorCards.Where(bb => product.color_cards.Contains(bb.Id) &&
                !existingProdCCards.Any(cc => cc.Id == bb.Id)))
            {
                prod.ColorCards.Add(c);
            }

            trans.Name = product.Name;
            trans.Remark = product.Remark;
            trans.ShortDescription = product.ShortDescription;
            trans.FullDescription = product.FullDescription;

            prod.UpdateAt = DateTime.UtcNow;
            prod.UpdateBy = User.Identity.Name;

            var imageUpdated = false;
            if (product.submit_image != null && product.submit_image.ContentLength > 0)
            {
                var img = FileUpload(product.submit_image, Conf.ProductImagePath, Conf.ProductImageThumbnailSize);
                if (img.ContainsKey("Image"))
                {
                    prod.Image = img["Image"];
                    prod.ImageThumb = img["Thumbnail"];
                    imageUpdated = true;
                }
            }

            if (_db.SaveChanges() > 0)
            {
                if (imageUpdated)
                {
                    if (System.IO.File.Exists(origImg)) System.IO.File.Delete(origImg);
                    if (System.IO.File.Exists(origThumb)) System.IO.File.Delete(origThumb);
                }

                TempData["Notice"] = Products.Notice_ProductUpdatedSuccessfully;
                return RedirectToActionPermanent("index");
            }

            TempData["Error"] = Products.Error_UnableToUpdateProduct;

            return View("edit", product);
        }

        public ActionResult Delete(int id = 0)
        {
            Product product = _db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        //
        // POST: /Products/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = _db.Products.Find(id);
            _db.Products.Remove(product);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}