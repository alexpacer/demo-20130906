﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project20130906.App_LocalResources.Views {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Common {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Common() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Project20130906.App_LocalResources.Views.Common", typeof(Common).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string btn_delete {
            get {
                return ResourceManager.GetString("btn_delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Modify.
        /// </summary>
        public static string btn_modify {
            get {
                return ResourceManager.GetString("btn_modify", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wrong image file format provided.
        /// </summary>
        public static string Error_ImageFileIsInWrongFormat {
            get {
                return ResourceManager.GetString("Error_ImageFileIsInWrongFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hello!.
        /// </summary>
        public static string login_hello {
            get {
                return ResourceManager.GetString("login_hello", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome..
        /// </summary>
        public static string login_welcome {
            get {
                return ResourceManager.GetString("login_welcome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First.
        /// </summary>
        public static string nav_firstPage {
            get {
                return ResourceManager.GetString("nav_firstPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last.
        /// </summary>
        public static string nav_lastPage {
            get {
                return ResourceManager.GetString("nav_lastPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next.
        /// </summary>
        public static string nav_nextPage {
            get {
                return ResourceManager.GetString("nav_nextPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to page {0}/{1} of {2} records..
        /// </summary>
        public static string nav_pageIndex {
            get {
                return ResourceManager.GetString("nav_pageIndex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prev.
        /// </summary>
        public static string nav_prevPage {
            get {
                return ResourceManager.GetString("nav_prevPage", resourceCulture);
            }
        }
    }
}
