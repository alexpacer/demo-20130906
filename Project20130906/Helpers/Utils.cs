﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.Win32;

namespace Project20130906.Helpers
{
    public class Utils
    {
        public static string GetMimeType(FileInfo fileInfo)
        {
            string mimeType = "application/unknown";

            RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(
                fileInfo.Extension.ToLower()
                );

            if (regKey != null)
            {
                object contentType = regKey.GetValue("Content Type");

                if (contentType != null)
                    mimeType = contentType.ToString();
            }

            return mimeType;
        }


        public static long DateTimeToPosixTimestamp(DateTime dateTime)
        {
            TimeSpan unixTimeSpan = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)unixTimeSpan.TotalSeconds;
        }
    }

}