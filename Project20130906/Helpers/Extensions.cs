﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Project20130906.Libs.Entities;
using System.Configuration;

namespace Project20130906.Helpers
{
    public static class Extensions
    {
        public static string DefaultLanguage = ConfigurationManager.AppSettings["DefaultLanguage"];

        public static bool IsImage(this HttpPostedFileBase file)
        {
            var formats = new[] { "jpg", "png", "gif", "jpeg" };

            return formats.Any(x => file.ContentType.Contains(x));
        }

        public static CategoryTranslation Display(this ICollection<CategoryTranslation> t, string language)
        {
            if (string.IsNullOrEmpty(language))
                language = DefaultLanguage;
            if (t == null || t.Count == 0) return null;
            var trans = t.FirstOrDefault(x => x.Language == language);

            if (trans == null)
                return null;

            return trans;
        }

        public static ColorCardGroupTranslation Display(this ICollection<ColorCardGroupTranslation> t, string language)
        {
            if (string.IsNullOrEmpty(language))
                language = DefaultLanguage;
            if (t == null || t.Count == 0) return null;
            var trans = t.FirstOrDefault(x => x.Language == language);

            if (trans == null)
                return null;

            return trans;
        }
    }
}