﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Project20130906.App_LocalResources.Views;

namespace Project20130906.Helpers
{
    public static class I18nHelper
    {
        public static string MenuTranslation(string key)
        {
            return Menu.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true)
                               .FetchByKey(key);
        }

        public static string FetchByKey(this ResourceSet set, string key)
        {
            var d = (from DictionaryEntry entry in set
                     where entry.Key.ToString() == key
                     select entry).FirstOrDefault();
            if (d.Value != null)
                return d.Value.ToString();
            return "";
        }
    }
}