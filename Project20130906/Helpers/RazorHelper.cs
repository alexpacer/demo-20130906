﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Project20130906.Libs;

namespace Project20130906.Helpers
{
    public static class RazorHelper
    {
        //public static MvcHtmlString RouteUrl(this HtmlHelper helper, string routeName)
        //{
        //    //helper.RouteCollection.Clear();
        //    //helper.RouteCollection.Remove(helper.RouteCollection["controller"]);
        //    helper.RouteCollection.Remove(helper.RouteCollection["action"]);
        //    helper.RouteCollection.Remove(helper.RouteCollection["id"]);

        //    var url = new UrlHelper(helper.ViewContext.RequestContext);
        //    return new MvcHtmlString(url.RouteUrl(url));
        //}

        public static MvcHtmlString DisplayImage(string image, bool isThumb = true)
        {
            var realPath = HttpContext.Current.Server.MapPath(image);
            if (System.IO.File.Exists(realPath))
            {
                var vPath = VirtualPathUtility.ToAbsolute(image);
                return new MvcHtmlString(string.Format(@"<img src='{0}' />", vPath));
            }
            else
            {
                string noImgPath = (isThumb)
                                       ? VirtualPathUtility.ToAbsolute(Conf.DefaultImageThumbPath)
                                       : VirtualPathUtility.ToAbsolute(Conf.DefaultImagePath);
                return new MvcHtmlString(string.Format(@"<img src='{0}' />", noImgPath));
            }
        }


        public static MvcHtmlString Image(this HtmlHelper html, string imagePath, string alt="")
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            // build the <img> tag
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            // build the <a> tag
            //var anchorBuilder = new TagBuilder("a");
            //anchorBuilder.MergeAttribute("href", url.Action(action, routeValues));
            //anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            //string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(imgHtml);
        }
    }
}