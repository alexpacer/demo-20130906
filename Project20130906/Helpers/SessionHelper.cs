﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Project20130906.Helpers
{
    public static class SessionHelper
    {
        #region Private methods
        /// <summary>
        /// Read session variable of the given type T
        /// </summary>
        /// <typeparam name="T">the type of the variable being stored</typeparam>
        /// <param name="key">the session key that's storing the variable</param>
        /// <returns>the type casted object of the variable</returns>
        private static T ReadSession<T>(string key)
        {
            if (HttpContext.Current.Session != null)
            {
                var obj = HttpContext.Current.Session[key];
                if (obj != null)
                {
                    return (T)obj;
                }
            }
            return default(T);
        }

        /// <summary>
        /// Store give object into session.
        /// * the object must be serializable.
        /// </summary>
        /// <typeparam name="T">the type of the object</typeparam>
        /// <param name="key">the key of the session</param>
        /// <param name="value">the object to be stored</param>
        private static void SetSession<T>(string key, T value)
        {
            if (value == null)
                HttpContext.Current.Session.Remove(key);
            else
                HttpContext.Current.Session[key] = value;
        }

        /// <summary>
        /// Read string data from cookie
        /// </summary>
        /// <param name="key">the key to retreive its value</param>
        /// <returns>the value stored in cookie</returns>
        private static string ReadCookie(string key)
        {
            return HttpContext.Current.Request.Cookies[key] != null ? HttpContext.Current.Request.Cookies[key].Value : string.Empty;
        }

        /// <summary>
        /// Store give value in cookie
        /// </summary>
        /// <param name="key">the key of the cookie</param>
        /// <param name="value">the string value to be stored</param>
        private static void SetCookie(string key, string value)
        {
            if (value == null)
            {
                HttpContext.Current.Response.Cookies.Remove(key);
            }
            else
            {
                var httpCookie = HttpContext.Current.Response.Cookies[key];
                if (httpCookie != null)
                {
                    httpCookie.Value = value;
                    httpCookie.Expires = DateTime.MaxValue;
                }
            }
        }
        #endregion

        public static string CurrentLanguage
        {
            get
            {
                string lang = ConfigurationManager.AppSettings["DefaultLanguage"];
                string cookieLang = ReadCookie("lan");

                if (HttpContext.Current.Request.Browser.Cookies &&
                    !string.IsNullOrEmpty(cookieLang))
                    lang = cookieLang;

                SetCookie("lan", lang);

                return lang;
            }
            set
            {
                SetCookie("lan", value);
            }
        }

        public static int PageSize
        {
            get
            {
                int def;
                if (!Int32.TryParse(ReadCookie("page_size"), out def) || def == 0)
                    def = 50;

                return def;
            }
            set { SetCookie("page_size", value.ToString(CultureInfo.InvariantCulture)); }
        }

        public static int[] UserPermissions
        {
            get
            {
                try
                {
                    return (int[])ReadSession<int[]>("perm");
                }
                catch (Exception)
                {
                    return new int[]{};
                }
            }

            set
            {
                SetSession("perm", value);
            }
        }
    }
}