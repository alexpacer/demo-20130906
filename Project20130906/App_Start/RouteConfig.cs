﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Project20130906.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "products",
                url: "products/{action}/{id}",
                defaults: new { controller = "Products", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "categories",
                url: "categories/{action}/{id}",
                defaults: new { controller = "Categories", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "color-cards",
                url: "color-cards/{action}/{id}",
                defaults: new { controller = "ColorCards", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "color-cards-groups",
                url: "color-card/groups/{action}/{id}",
                defaults: new { controller = "ColorCardGroups", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "news",
                url: "news/{action}/{id}",
                defaults: new { controller = "News", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "permissions",
                url: "permissions/{action}/{id}",
                defaults: new { controller = "Permissions", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "install",
                url: "install",
                defaults: new { controller = "Setup", action = "Install" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "News", action = "home", id = UrlParameter.Optional }
            );
        }
    }
}