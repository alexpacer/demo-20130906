﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;

namespace Project20130906.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);



            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            config.Routes.MapHttpRoute(
                name: "api-language-preference",
                routeTemplate: "api/language-preference",
                defaults: new { controller = "WebApi", action = "LanguagePreference", p = RouteParameter.Optional }//,
                //constraints: new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { HttpMethod.Post }.ToString()) }
            );

            config.Routes.MapHttpRoute(
                name: "api-user-lookup",
                routeTemplate: "api/user-lookup/{username}",
                defaults: new { controller = "WebApi", action = "UserLookup", username = RouteParameter.Optional }//,
                //constraints: new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { HttpMethod.Post }.ToString()) }
            );

            config.Routes.MapHttpRoute(
                name: "api-user-add-permission",
                routeTemplate: "api/user/permission",
                defaults: new { controller = "WebApi", action = "UserAddPermission" }//,
                //constraints: new { httpMethod = new HttpMethodConstraint(new HttpMethod[] { HttpMethod.Post }.ToString()) }
            );

            config.Routes.MapHttpRoute(
                name: "api-users-add-permission",
                routeTemplate: "api/users/permissions",
                defaults: new { controller = "WebApi", action = "UsersAddPermission" }//,
            );
            


            config.Routes.MapHttpRoute(
                name: "api-user-remove-permission",
                routeTemplate: "api/remove-permissions",
                defaults: new { controller = "WebApi", action = "RemoveUserPermission" },
                constraints: new { httpMethod = new HttpMethodConstraint("POST") }
            );

            config.Routes.MapHttpRoute(
                name: "api-delete-news",
                routeTemplate: "api/news/{id}",
                defaults: new { controller = "WebApi", action = "NewsDelete" }
            );

            config.Routes.MapHttpRoute(
                name: "api-commit-install",
                routeTemplate: "api/commit-install",
                defaults: new { controller = "WebApi", action = "CommitInstall" }
            );

            #region Category

            config.Routes.MapHttpRoute(
                name: "api-order-category",
                routeTemplate: "api/order-category",
                defaults: new { controller = "WebApi", action = "UpdateCategoryOrder" },
                constraints: new { httpMethod = new HttpMethodConstraint("POST") }
            );

            config.Routes.MapHttpRoute(
                name: "api-update-category",
                routeTemplate: "api/categories/{id}",
                defaults: new { controller = "WebApi", action = "UpdateCategory" },
                constraints: new { httpMethod = new HttpMethodConstraint("PUT") }
            );

            config.Routes.MapHttpRoute(
                name: "api-upload-image-category",
                routeTemplate: "api/categories/{category_id}/images",
                defaults: new { controller = "WebApi", action = "UploadCategoryImage" },
                constraints: new { httpMethod = new HttpMethodConstraint("POST") }
            );

            config.Routes.MapHttpRoute(
                name: "api-delete-image-category",
                routeTemplate: "api/categories/images/{file}",
                defaults: new { controller = "WebApi", action = "DeleteCategoryImageFile" },
                constraints: new { httpMethod = new HttpMethodConstraint("POST") }
            );
            

            config.Routes.MapHttpRoute(
                name: "api-delete-category",
                routeTemplate: "api/categories/{id}",
                defaults: new { controller = "WebApi", action = "DeleteCategory" },
                constraints: new { httpMethod = new HttpMethodConstraint("DELETE") }
            );

            config.Routes.MapHttpRoute(
                name: "api-all-categories",
                routeTemplate: "api/categories",
                defaults: new { controller = "WebApi", action = "AllCategories" },
                constraints: new { httpMethod = new HttpMethodConstraint("GET") }
            );

            #endregion

            config.Routes.MapHttpRoute(
                name: "api-delete-product",
                routeTemplate: "api/products/{id}",
                defaults: new { controller = "WebApi", action = "DeleteProduct" },
                constraints: new { httpMethod = new HttpMethodConstraint("DELETE") }
            );
            

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}