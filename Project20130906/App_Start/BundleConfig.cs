﻿using System.Web.Optimization;

namespace Project20130906.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                "~/Scripts/site.js"));

            #region jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jQuery/jquery-1.8.2.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jQuery/jquery-ui-1.10.3.custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jQuery/jquery.unobtrusive*",
                        "~/Scripts/jQuery/jquery.validate*"));

            #endregion

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr/modernizr-*"));

            #region jHtmlArea
            bundles.Add(new ScriptBundle("~/bundles/jhtmlarea").Include(
                "~/Scripts/jHtmlArea/jHtmlArea-0.7.5.js",
                "~/Scripts/jHtmlArea/jHtmlArea.ColorPickerMenu-0.7.0.js",
                "~/Scripts/jHtmlArea/init.js"));

            bundles.Add(new StyleBundle("~/Content/jhtmlarea/jhtmlarea").Include(
                    "~/Content/jHtmlArea/jHtmlArea.css",
                    //"~/Content/jHtmlArea/jHtmlArea.Editor.css",
                    "~/Content/jHtmlArea/jHtmlArea.ColorPickerMenu.css"
                ));
            #endregion

            #region jquery_multiselect
            bundles.Add(new ScriptBundle("~/bundles/jquery.multiselect").Include(
                "~/Scripts/jQuery.multiselect/jquery.multiselect*"
                ));

            bundles.Add(new StyleBundle("~/Content/jQuery.multiselect")
                .Include("~/Content/jQuery.multiselect/jquery.multiselect.css"));
            #endregion

            #region jQuery.FileUpload
            bundles.Add(new ScriptBundle("~/bundles/jquery.fileupload").Include(
                "~/Scripts/jQuery.FileUpload/vendor/jquery.ui.widget.js",
                "~/Scripts/jQuery.FileUpload/jquery.iframe-transport.js",
                "~/Scripts/jQuery.FileUpload/jquery.fileupload.js"
                ));
            #endregion

            bundles.Add(new StyleBundle("~/Content/site")
                //.Include("~/Content/site.css")
                .Include("~/Content/init-controls.css"));


            #region bootstrap
            bundles.Add(new StyleBundle("~/Content/bootstrap/bootstrap")
                .Include(
                    "~/content/bootstrap/bootstrap.css",
                    "~/content/bootstrap/bootstrap-responsive.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include(
                    "~/scripts/bootstrap/bootstrap.js",
                    "~/scripts/bootstrap/bootbox.*"));
            #endregion

            #region Bootstrap multi select
            bundles.Add(new StyleBundle("~/Content/bootstrap-multiselect")
                .Include(
                    "~/content/bootstrap/bootstrap-multiselect.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap.multiselect")
                .Include("~/scripts/bootstrap/bootstrap-multiselect.js"));
            
            #endregion

        }
    }
}