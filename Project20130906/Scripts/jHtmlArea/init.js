﻿$(function () {
    $("[data-js='rich-text-editor']").htmlarea({
        toolbar: [
            ['html'],
            ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript'],
            ['increasefontsize', 'decreasefontsize'],
            ['orderedList', 'unorderedList'],
            ['indent', 'outdent'],
            ['justifyleft', 'justifycenter', 'justifyright'],
            ['link', 'unlink', 'image', 'horizontalrule'],
            ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
            ['forecolor'],
            ['cut', 'copy', 'paste']
        ],
        loaded: function() {
            $(".jHtmlArea iframe").removeAttr('style').addClass("jhtmleditor-customisation");
        }
    });

    $(".ToolBar").width("500px");

});