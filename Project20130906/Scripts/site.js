﻿/**
 * Fixes IE trim() method. 
 **/
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

var ie = (function () {
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');

    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );

    /*@cc_on
    if (/^10/.test(@_jscript_version)) {
        v = 10;
    }
    @*/

    return v > 4 ? v : undef;
}());



/**
 * Adds 0 left margin to the first thumbnail on each row that don't get it via CSS rules.
 * Recall the function when the floating of the elements changed.
 */
function fixThumbnailMargins() {
    $('.row-fluid .thumbnails').each(function () {
        if ($(this).children().first().offset() == undefined) return;
        var $thumbnails = $(this).children(),
            previousOffsetLeft = $thumbnails.first().offset().left;
        $thumbnails.removeClass('first-in-row');
        $thumbnails.first().addClass('first-in-row');
        $thumbnails.each(function () {
            var $thumbnail = $(this),
                offsetLeft = $thumbnail.offset().left;
            if (offsetLeft < previousOffsetLeft) {
                $thumbnail.addClass('first-in-row');
            }
            previousOffsetLeft = offsetLeft;
        });
    });
}

/**
 * specific initialize color card popover element
 */
function initColorCardPopOver() {
    var tmp = $.fn.popover.Constructor.prototype.show;
    $.fn.popover.Constructor.prototype.show = function () {
        var $e = this.$element;
        this.options.html = 'true';
        this.options.placement = 'right';
        this.options.content = $("#image-popover_" + $e.attr("data-hash")).html();
        tmp.call(this);
    };
}

/**
 * initialize color card group selection editor element
 */
function initColorCardGrpupEditor() {
    $("[data-js='select-colorcard-group']")
                .each(function () {
                    var groupId = $(this).attr("data-id");
                    var fellowCards = $("[data-group-id='" + groupId + "']");
                    var selectedFellowCards = $("[data-group-id='" + groupId + "']:checked");
                    if (selectedFellowCards.length != 0) {
                        $(this).parents(".accordion-toggle").addClass("sub-colorcard-selected");
                    }

                    if (selectedFellowCards.length == fellowCards.length)
                        $(this).attr("checked", "checked");
                });

    $("[data-js='select-colorcard-group']")
        .change(function () {
            var groupId = $(this).attr("data-id");
            var fellowCards = $("[data-group-id='" + groupId + "']");
            if (this.checked) {
                fellowCards.each(function () {
                    $(this).attr("checked", "checked");
                });
            } else {
                fellowCards.each(function () {
                    $(this).removeAttr("checked");
                });
            }
        })
        .bootstrapToolTip();
}

(function (window) {
    $.fn.extend({
        parseResponse: function (opt) {
            var e = $(this);
            var def = {
                category: 'general'
            };
            def = $.extend(def, opt);

            if (e[0].message == undefined || e[0].message == '') { return; }

            if (e[0].status == 'error') {
                var errSel = $("[data-js='error']");
                errSel.html(e[0].message);
                errSel.show('fast');
            } else if (e[0].status == 'success') {
                var errSel = $("[data-js='notice']");
                errSel.html(e[0].message);
                errSel.show('fast');
                
                if (def.url != undefined && def.url != '') {
                    setTimeout(function() {
                        location.href = def.url;
                    }, 2000);
                } else if (typeof def.success != 'undefined') {
                    def.success();
                }

            }
        },
        twitterPopover: function() {
            $(this).mouseover(function (e) {
                e.preventDefault();
                $(this).popover('show');
            })
            .mouseout(function (e) {
                e.preventDefault();
                $(this).popover('hide');
            });
            return $(this);
        },
        bootstrapToolTip: function() {
            $(this).mouseover(function (e) {
                e.preventDefault();
                $(this).tooltip('show');
            })
            .mouseout(function (e) {
                e.preventDefault();
                $(this).tooltip('hide');
            });
            return $(this);
        }
    });
})(jQuery);




$(document).ready(function () {
    
    if ($("[data-js='notice']").html().trim().length) {
        $("[data-js='notice']").show('fast');
    }

    if ($("[data-js='error']").html().trim().length) {
        $("[data-js='error']").show('fast');
    }

    $("[data-js='select-language']").change(function (e) {
        console.log($("input[name='language-picker-url']").val());
        var target = $(e.target);
        $.ajax({
            url: $("input[name='language-picker-url']").val(),
            type: 'POST',
            data: { p: target.val() }
        }).success(function (data) {
            console.log(data);
            console.log(target);
            location.reload();
        });
    });

    fixThumbnailMargins();
});

