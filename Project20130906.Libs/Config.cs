using System;
using System.Configuration;

namespace Project20130906.Libs
{
    public static class Conf
    {
        /// <summary>
        /// Returns the domain user's logon mode {machine|LDAP} from configuration
        /// </summary>
        public static string UserLookupMode
        {
            get { return string.Format("{0}", ConfigurationManager.AppSettings["UserLookupMode"]).ToLower(); }
        }

        /// <summary>
        /// Path for category background images storage
        /// </summary>
        public static string CategoryBackgroundPath
        {
            get { return ConfigurationManager.AppSettings["CategoryBackgroundPath"]; }
        }

        /// <summary>
        /// The thumbnail width of the uploaded Category Background image that'll be created. Default:70
        /// </summary>
        public static int CategoryBackgroundThumbnailSize
        {
            get
            {
                try
                {
                    int ret;
                    Int32.TryParse(ConfigurationManager.AppSettings["CategoryBackgroundImageThumbSize"], out ret);
                    return ret;
                }
                catch (Exception)
                {
                    return 70;
                }
            }
        }

        #region Color Card Cover Image
        /// <summary>
        /// Path that points to color card group cover image directory
        /// </summary>
        public static string ColorCardGroupCoverImagePath
        {
            get { return ConfigurationManager.AppSettings["ColorCardGroupCoverImagePath"]; }
        }

        /// <summary>
        /// The thumbnail width of the uploaded Color Card Cover image that'll be created. Default:80
        /// </summary>
        public static int ColorCardGroupCoverImageThumbnailSize
        {
            get
            {
                try
                {
                    int ret;
                    Int32.TryParse(ConfigurationManager.AppSettings["ColorCardGroupCoverImageThumbnailSize"], out ret);
                    return ret;
                }
                catch (Exception)
                {
                    return 80;
                }
            }
        }
        #endregion

        /// <summary>
        /// Path that points to color card images directory
        /// </summary>
        public static string ColorCardImagePath
        {
            get { return ConfigurationManager.AppSettings["ColorCardBackgroundPath"]; }
        }


        /// <summary>
        /// The thumbnail width of the uploaded Color Card image that'll be created. Default:70
        /// </summary>
        public static int ColorCardThumbnailSize
        {
            get
            {
                try
                {
                    int ret;
                    Int32.TryParse(ConfigurationManager.AppSettings["ColorCardThumbnailSize"], out ret);
                    return ret;
                }
                catch (Exception)
                {
                    return 70;
                }
            }
        }

        public static string CategoryCoverImagePath
        {
            get { return ConfigurationManager.AppSettings["CategoryCoverPath"]; }
        }

        /// <summary>
        /// The thumbnail width of the uploaded Category Cover image that'll be created. Default:70
        /// </summary>
        public static int CategoryCoverThumbnailSize
        {
            get
            {
                try
                {
                    int ret;
                    Int32.TryParse(ConfigurationManager.AppSettings["CategoryCoverImageThumbSize"], out ret);
                    return ret;
                }
                catch (Exception)
                {
                    return 70;
                }
            }
        }

        public static string ProductImagePath
        {
            get { return ConfigurationManager.AppSettings["ProductImagePath"]; }
        }

        /// <summary>
        /// The thumbnail width of the uploaded Product image that'll be created. Default:70
        /// </summary>
        public static int ProductImageThumbnailSize
        {
            get
            {
                try
                {
                    int ret;
                    Int32.TryParse(ConfigurationManager.AppSettings["ProductImageThumbnailSize"], out ret);
                    return ret;
                }
                catch (Exception)
                {
                    return 70;
                }
            }
        }

        public static string CategorySloganImagePath
        {
            get { return ConfigurationManager.AppSettings["CategorySloganImagePath"]; }
        }

        /// <summary>
        /// The thumbnail width of the uploaded Category Slogan image that'll be created. Default:120
        /// </summary>
        public static int CategorySloganImageThumbSize
        {
            get
            {
                try
                {
                    int ret;
                    Int32.TryParse(ConfigurationManager.AppSettings["CategorySloganImageThumbSize"], out ret);
                    return ret;
                }
                catch (Exception)
                {
                    return 120;
                }
            }
        }


        /// <summary>
        /// Configuration to debug mode that will display all menu items disregard users' priviedge
        /// </summary>
        public static bool DisplayAllMenus
        {
            get { return (string.Format((string)@"{0}", (object)ConfigurationManager.AppSettings["DisplayAllMenus"]).ToLower() == "y"); }
        }

        /// <summary>
        /// Language codes available in system
        /// </summary>
        public static string[] LanguageCodes
        {
            get { return ConfigurationManager.AppSettings["LanguageCodes"].Split(','); }
        }

        /// <summary>
        /// Default LDAP server to connect to for user lookup
        /// </summary>
        public static string LDAPServer
        {
            get { return ConfigurationManager.AppSettings["LDAP_Server"]; }
        }

        /// <summary>
        /// The default thumbnail width 
        /// </summary>
        public static string DefaultImageThumbPath
        {
            get { return ConfigurationManager.AppSettings["DefaultImageThumbPath"]; }
        }

        /// <summary>
        /// The default path that is used to store image
        /// </summary>
        public static string DefaultImagePath
        {
            get { return ConfigurationManager.AppSettings["DefaultImagePath"]; }
        }

        #region Sample
        /// <summary>
        /// Where Sample color card images are stored when creating initial dummy data
        /// </summary>
        public static string SampleColorCardDirectory
        {
            get { return ConfigurationManager.AppSettings["SampleColorCardDirectory"]; }
        }

        /// <summary>
        /// Where Sample category background images are stored when creating initial dummy data
        /// </summary>
        public static string SampleCategoryBackgroundDirectory
        {
            get { return ConfigurationManager.AppSettings["SampleCategoryBackgroundDirectory"]; }
        }

        /// <summary>
        /// Where Sample Category Cover images are stored when creating initial dummy data
        /// </summary>
        public static string SampleCategoryCoverDirectory
        {
            get { return ConfigurationManager.AppSettings["SampleCategoryCoverDirectory"]; }
        }

        /// <summary>
        /// Where Sample Product images are stored when creating initial dummy data
        /// </summary>
        public static string SampleProductImageDirectory
        {
            get { return ConfigurationManager.AppSettings["SampleProductImageDirectory"]; }
        }

        /// <summary>
        /// Max length of category description allowed from user input
        /// </summary>
        public static decimal CategoryDescriptionMaxLength
        {
            get
            {
                int def = 160;
                Int32.TryParse(ConfigurationManager.AppSettings["CategoryDescriptionMaxLength"], out def);
                return def;
            }
        }

        #endregion


    }
}