﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project20130906.Libs
{
    public static class Def
    {
        /// <summary>
        /// Permission tables of each rolw's permission id
        /// </summary>
        public enum Permissions
        {
            None = 0,
            God = 1,
            Product_Management = 2,
            Product_Amendment = 3,
            Category_Management = 4,
            Category_Amendment = 5,
            Color_Card_Management = 6,
            News_Management = 7,
            News_Amendment = 8,
            Outlook_Web_Access = 9,
            Manage_Permissions = 10
        }
    }
}
