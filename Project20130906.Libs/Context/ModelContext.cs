using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Project20130906.Libs.Context.Mappings;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context
{
    public class ModelContext : DbContext
    {
        public ModelContext() : base("ModelContext")
        {
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ColorCard> ColorCards { get; set; }
        public DbSet<ColorCardGroup> ColorCardGroups { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<NewsPost> NewsPosts { get; set; }
        //public DbSet<Function> Functions { get; set; }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            base.OnModelCreating(mb);
            mb.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            mb.Configurations.Add(new CategoryMapping());
            mb.Configurations.Add(new CategoryTranslationMapping());
            mb.Configurations.Add(new ColorCardGroupTranslationMapping());
            mb.Configurations.Add(new ColorCardGrpoupMapping());
            mb.Configurations.Add(new ColorCardMapping());
            mb.Configurations.Add(new NewsPostMapping());
            mb.Configurations.Add(new PermissionMapping());
            mb.Configurations.Add(new ProductMapping());
            mb.Configurations.Add(new UserMapping());
        }

        public void CreateDb()
        {
            Database.CreateIfNotExists();
        }

        public int ExecuteSql(string sql)
        {
            return this.Database.ExecuteSqlCommand(sql);
        }

        public void DeleteDb()
        {
            Database.Delete();
        }
    }


}