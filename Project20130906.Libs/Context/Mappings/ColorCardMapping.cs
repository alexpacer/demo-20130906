﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context.Mappings
{
    public class ColorCardMapping : EntityTypeConfiguration<ColorCard>
    {
        public ColorCardMapping()
        {
            this.HasOptional(x=>x.Group)
                .WithMany(x=>x.ColorCards)
                .HasForeignKey(x=>x.GroupId)
                .WillCascadeOnDelete(false);
        }
    }
}
