using System.Data.Entity.ModelConfiguration;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context.Mappings
{
    public class PermissionMapping : EntityTypeConfiguration<Permission>
    {
        public PermissionMapping()
        {
            this.HasMany(x => x.Users)
                .WithMany(x => x.Permissions);

        }
    }
}