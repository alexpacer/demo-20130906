using System.Data.Entity.ModelConfiguration;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context.Mappings
{
    public class CategoryTranslationMapping : EntityTypeConfiguration<CategoryTranslation>
    {
        public CategoryTranslationMapping()
        {
            this.HasKey(p => new { p.Id, p.Language });

            this.Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();

        }
    }
}