using System.Data.Entity.ModelConfiguration;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context.Mappings
{
    public class UserMapping : EntityTypeConfiguration<User>
    {
        public UserMapping()
        {
            this.HasKey(x => x.Id);

            this.HasMany(x => x.NewsPosts)
                .WithRequired(x => x.Creator)
                .HasForeignKey(x => x.CreatedBy)
                .WillCascadeOnDelete(true);

            this.HasMany(x => x.CreatedProducts)
                .WithRequired(x => x.Creator)
                .HasForeignKey(x => x.CreatedBy)
                .WillCascadeOnDelete(true);

            this.HasMany(x => x.CreatedColorCards)
                .WithRequired(x => x.Creator)
                .HasForeignKey(x => x.CreatedBy)
                .WillCascadeOnDelete(true);
        }
    }
}