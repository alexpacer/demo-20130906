using System.Data.Entity.ModelConfiguration;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context.Mappings
{
    public class NewsPostMapping : EntityTypeConfiguration<NewsPost>
    {
        public NewsPostMapping()
        {
            this.HasMany(x => x.Translations)
                .WithRequired(x => x.NewsPost)
                .HasForeignKey(x => x.NewsPostId)
                .WillCascadeOnDelete(true);

        }
    }
}