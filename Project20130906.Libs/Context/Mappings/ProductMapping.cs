using System.Data.Entity.ModelConfiguration;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context.Mappings
{
    public class ProductMapping : EntityTypeConfiguration<Product>
    {
        public ProductMapping()
        {
            this.HasKey(x => x.Id);


            this.HasMany(x => x.Translations)
                .WithRequired(x => x.Product)
                .HasForeignKey(x => x.ProductId)
                .WillCascadeOnDelete(true);

            this.HasMany(p => p.Categories)
                .WithMany(c => c.Products)
                .Map(mc =>
                    {
                        mc.ToTable("ProductsToCategories");
                        mc.MapLeftKey("ProductId");
                        mc.MapRightKey("Categories");
                    });

            this.HasMany(x => x.ColorCards)
                .WithMany(x => x.Products)
                .Map(mc =>
                    {
                        mc.ToTable("ProductsToColorCards");
                        mc.MapLeftKey("ProductId");
                        mc.MapRightKey("ColorCardId");
                    });
        }
    }
}