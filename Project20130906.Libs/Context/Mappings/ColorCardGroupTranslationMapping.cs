﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project20130906.Libs.Entities;

namespace Project20130906.Libs.Context.Mappings
{
    public class ColorCardGroupTranslationMapping :
        EntityTypeConfiguration<ColorCardGroupTranslation>
    {
        public ColorCardGroupTranslationMapping()
        {
            this.HasKey(p => new {p.Id, p.Language});

            this.Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();
        }
    }
}
