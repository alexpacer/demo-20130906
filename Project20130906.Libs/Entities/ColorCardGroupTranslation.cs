﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Project20130906.Libs.Entities
{
    public class ColorCardGroupTranslation
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public string Name { get; set; }

        [Required]
        [Column(Order = 2)]
        public string Language { get; set; }

        [Required]
        [Column(Order = 3)]
        public int ColorCardGroupId { get; set; }

        public virtual ColorCardGroup ColorCardGroup { get; set; }
    }
}
