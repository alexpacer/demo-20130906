﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Project20130906.Libs.Entities
{
    public class ColorCardGroup
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public string Image { get; set; }

        [Column(Order = 2)]
        public string ImageThumb { get; set; }

        public virtual ICollection<ColorCard> ColorCards { get; set; }
        public virtual ICollection<ColorCardGroupTranslation> Translations { get; set; }

        public ColorCardGroupTranslation GetTranslation(string language)
        {
            var trans = this.Translations.FirstOrDefault(t => t.Language == language) ??
                        this.Translations.FirstOrDefault();

            return trans;
        }
    }
}
