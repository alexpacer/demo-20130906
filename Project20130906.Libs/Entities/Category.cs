using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Project20130906.Libs.Entities
{
    public class Category
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public int Order { get; set; }

        [Column(Order = 2)]
        public int? ParentId { get; set; }

        [Column(Order = 3)]
        public string BackgroundImage { get; set; }
        [Column(Order = 4)]
        public string BackgroundImageThumb { get; set; }

        [Column(Order = 5)]
        public string CoverImage { get; set; }

        [Column(Order = 6)]
        public string CoverImageThumb { get; set; }

        [Required]
        [Column(Order = 7)]
        public DateTime CreateAt { get; set; }

        [Required]
        [Column(Order = 8)]
        public string CreatedBy { get; set; }

        [Column(Order = 9)]
        public string UpdatedBy { get; set; }

        [Column(Order = 10)]
        public DateTime? UpdatedAt { get; set; }

        [Column(Order = 11)]
        public string SloganImage { get; set; }
        [Column(Order = 12)]
        public string SloganImageThumb { get; set; }

        public virtual Category Parent { get; set; }
        public virtual ICollection<Category> Children { get; set; }
        public virtual ICollection<CategoryTranslation> Translations { get; set; }
        public virtual ICollection<Product> Products { get; set; }

        public CategoryTranslation GetTranslation(string language)
        {
            var trans = this.Translations.FirstOrDefault(t => t.Language == language);
            if (trans == null)
                trans = this.Translations.FirstOrDefault();

            return trans;
        }
    }
}