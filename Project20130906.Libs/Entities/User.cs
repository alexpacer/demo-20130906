using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Project20130906.Libs.Entities
{
    public class User
    {
        [Column(Order = 0)]
        public string Id { get; set; }

        [Column(Order = 1)]
        public string Password { get; set; }

        [Column(Order = 2)]
        public string Email { get; set; }

        public bool HasPermission(int id)
        {
            return this.Permissions.Any(x => x.Id == id);
        }

        public virtual ICollection<Permission> Permissions { get; set; }
        public virtual ICollection<NewsPost> NewsPosts { get; set; }
        public virtual ICollection<Product> CreatedProducts { get; set; }
        public virtual ICollection<ColorCard> CreatedColorCards { get; set; }
    }
}