using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project20130906.Libs.Entities
{
    public class Permission
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        [MaxLength(255)]
        public string Name { get; set; }

        [Column(Order = 2)]
        public bool IsMenu { get; set; }

        [Column(Order = 3)]
        public string Route { get; set; }

        [Required]
        [Column(Order = 4)]
        public DateTime CreateAt { get; set; }

        [Required]
        [Column(Order = 5)]
        public string CreatedBy { get; set; }


        public virtual ICollection<User> Users { get; set; }
        
    }

}