using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Project20130906.Libs.Entities
{
    public class NewsPost
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(Order = 1)]
        public DateTime CreateAt { get; set; }

        [Required]
        [Column(Order = 2)]
        public string CreatedBy { get; set; }

        [Column(Order = 3)]
        public DateTime? ModifiedAt { get; set; }

        [Column(Order = 4)]
        public string ModifiedBy { get; set; }

        public virtual ICollection<NewsPostTranslation> Translations { get; set; }
        public virtual User Creator { get; set; }

        public NewsPostTranslation GetTranslation(string language)
        {
            var trans = this.Translations.FirstOrDefault(t => t.Language == language);
            if (trans == null)
                trans = this.Translations.FirstOrDefault();

            return trans;
        }
    }

    public class NewsPostTranslation
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public string Subject { get; set; }

        [Column(Order = 2)]
        public string Description { get; set; }

        [Required]
        [Column(Order = 3)]
        public string Language { get; set; }

        [Required]
        [Column(Order = 4)]
        public int NewsPostId { get; set; }

        public virtual NewsPost NewsPost { get; set; }
    }

}