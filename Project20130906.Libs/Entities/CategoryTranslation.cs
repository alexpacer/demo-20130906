using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project20130906.Libs.Entities
{
    public class CategoryTranslation
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public string Name { get; set; }

        [Column(Order = 2)]
        public string Slogan { get; set; }

        [Column(Order = 3)]
        public string Description { get; set; }

        [Required]
        [Column(Order = 4)]
        public string Language { get; set; }

        [Required]
        [Column(Order = 5)]
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
    }
}