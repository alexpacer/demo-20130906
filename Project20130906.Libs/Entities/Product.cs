using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Project20130906.Libs.Entities
{
    public class Product
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public string Image { get; set; }
        
        [Column(Order = 2)]
        public string ImageThumb { get; set; }

        [Required]
        [Column(Order = 3)]
        public DateTime CreateAt { get; set; }

        [Required]
        [Column(Order = 4)]
        public string CreatedBy { get; set; }

        [Column(Order = 5)]
        public string UpdateBy { get; set; }

        [Column(Order = 6)]
        public DateTime? UpdateAt { get; set; }

        public virtual User Creator { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<ProductTranslation> Translations { get; set; }
        public virtual ICollection<ColorCard> ColorCards { get; set; }

        public ProductTranslation GetTranslation(string language)
        {
            var trans = this.Translations.FirstOrDefault(t => t.Language == language);
            if (trans == null)
                trans = this.Translations.FirstOrDefault();

            return trans;
        }
    }

    public class ProductTranslation 
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public string Name { get; set; }

        [Column(Order = 2)]
        public string ShortDescription { get; set; }

        [Column(Order = 3)]
        public string FullDescription { get; set; }

        [Column(Order = 4)]
        public string Remark { get; set; }

        [Required]
        [Column(Order = 5)]
        public string Language { get; set; }

        [Required]
        [Column(Order = 6)]
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }

}