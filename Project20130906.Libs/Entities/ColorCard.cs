using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Project20130906.Libs.Entities
{
    public class ColorCard
    {
        [Column(Order = 0)]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        public string Name { get; set; }

        [Required]
        [Column(Order = 2)]
        public string Image { get; set; }

        [Required]
        [Column(Order = 3)]
        public string ImageThumb { get; set; }

        [Required]
        [Column(Order = 4)]
        public DateTime CreateAt { get; set; }

        [Required]
        [Column(Order = 5)]
        public string CreatedBy { get; set; }

        [Column(Order = 6)]
        public string UpdateBy { get; set; }

        [Column(Order = 7)]
        public DateTime? UpdateAt { get; set; }

        [Column(Order = 8)]
        public int? GroupId { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public virtual User Creator { get; set; }
        public virtual ColorCardGroup Group { get; set; }
    }
}